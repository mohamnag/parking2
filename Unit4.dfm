object MainPassF: TMainPassF
  Left = 352
  Top = 270
  BiDiMode = bdRightToLeft
  BorderStyle = bsDialog
  Caption = #1583#1585#1610#1575#1601#1578' '#1585#1605#1586' '#1575#1589#1604#1610
  ClientHeight = 129
  ClientWidth = 289
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 14
  object TntLabel1: TTntLabel
    Left = 15
    Top = 8
    Width = 266
    Height = 42
    Caption = 
      #1604#1591#1601#1575' '#1585#1605#1586' '#1575#1589#1604#1740' '#1585#1575' '#1608#1575#1585#1583' '#1606#1605#1575#1740#1740#1583'. '#1583#1585' '#1589#1608#1585#1578#1740' '#1705#1607' '#1575#1740#1606' '#1585#1605#1586' '#1585#1575' '#1606#1605#1740' '#1583#1575#1606#1740#1583' '#1576 +
      #1575#1740#1583' '#1576#1575' '#1576#1585#1606#1575#1605#1607' '#1606#1608#1740#1587' '#1608' '#1740#1575' '#1605#1575#1604#1705' '#1575#1589#1604#1740' '#1662#1575#1585#1705#1740#1606#1711' '#1578#1605#1575#1587' '#1576#1711#1740#1585#1740#1583
    Font.Charset = ARABIC_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
    Caption_UTF7 = 
      '+BkQGNwZBBic +BjEGRQYy +BicGNQZEBsw +BjEGJw +BkgGJwYxBi8 +BkYGRQ' +
      'YnBswGzAYv. +Bi8GMQ +BjUGSAYxBioGzA +BqkGRw +BicGzAZG +BjEGRQYy ' +
      '+BjEGJw +BkYGRQbM +Bi8GJwZGBswGLw +BigGJwbMBi8 +BigGJw +BigGMQZG' +
      'BicGRQZH +BkYGSAbMBjM +Bkg +BswGJw +BkUGJwZEBqk +BicGNQZEBsw +Bn' +
      '4GJwYxBqkGzAZGBq8 +BioGRQYnBjM +BigGrwbMBjEGzAYv'
  end
  object TntLabel2: TTntLabel
    Left = 228
    Top = 67
    Width = 51
    Height = 14
    Caption = #1585#1605#1586' '#1575#1589#1604#1740':'
    Caption_UTF7 = '+BjEGRQYy +BicGNQZEBsw:'
  end
  object MainPass: TTntEdit
    Left = 8
    Top = 64
    Width = 209
    Height = 22
    TabOrder = 0
    PasswordCharW = '*'
  end
  object TntButton2: TTntButton
    Left = 88
    Top = 96
    Width = 75
    Height = 25
    Caption = #1604#1594#1608
    ModalResult = 2
    TabOrder = 1
    Caption_UTF7 = '+BkQGOgZI'
  end
  object TntButton1: TTntButton
    Left = 8
    Top = 96
    Width = 73
    Height = 25
    Caption = #1576#1587#1610#1575#1585' '#1582#1608#1576
    ModalResult = 1
    TabOrder = 2
    Caption_UTF7 = '+BigGMwZKBicGMQ +Bi4GSAYo'
  end
end
