unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TntExtCtrls, Buttons, TntButtons, WinSkinData, Grids,
  DBGrids, TntDBGrids, StdCtrls, TntStdCtrls, MD5, Registry, UFarsiDate,
  DB, DBTables, RpDefine, RpRave, RpCon, RpConDS, qt, Menus, TntMenus,
  LMDCustomComponent, LMDGlobalHotKey;

type
  TMainF = class(TForm)
    RightPanel: TTntPanel;
    BasePanel: TTntPanel;
    InOutBut: TTntSpeedButton;
    GozBut: TTntSpeedButton;
    AjBut: TTntSpeedButton;
    TanBut: TTntSpeedButton;
    SkinData1: TSkinData;
    InputOutput: TTntPanel;
    TntButton10: TTntButton;
    TntShape7: TTntShape;
    SearchBCode: TTntEdit;
    TntLabel8: TTntLabel;
    TntDBGrid1: TTntDBGrid;
    newbut: TTntButton;
    TntLabel1: TTntLabel;
    SearchFieldS: TTntComboBox;
    TntButton11: TTntButton;
    TntShape1: TTntShape;
    TntButton1: TTntButton;
    TntButton2: TTntButton;
    TntDBGrid2: TTntDBGrid;
    TntButton3: TTntButton;
    SearchFieldSMashin: TTntComboBox;
    TntLabel2: TTntLabel;
    SearchBCodeMashin: TTntEdit;
    TntLabel3: TTntLabel;
    TntLabel4: TTntLabel;
    TntLabel5: TTntLabel;
    TntButton4: TTntButton;
    TntButton5: TTntButton;
    TntButton6: TTntButton;
    TntButton7: TTntButton;
    Reports: TTntPanel;
    TntShape2: TTntShape;
    TntLabel6: TTntLabel;
    NooZabtCB1: TTntComboBox;
    MotAndMashAmarMonth: TTntComboBox;
    NooZabtCB2: TTntComboBox;
    TntLabel12: TTntLabel;
    NooZabtCB3: TTntComboBox;
    NooZabtCB4: TTntComboBox;
    TntLabel15: TTntLabel;
    AnbarCB2: TTntComboBox;
    TntLabel22: TTntLabel;
    YeganCB2: TTntComboBox;
    TntButton14: TTntButton;
    TntLabel17: TTntLabel;
    MotOrMash: TTntComboBox;
    TntShape6: TTntShape;
    Tanzimat: TTntPanel;
    Ajans: TTntPanel;
    TntShape4: TTntShape;
    TntShape5: TTntShape;
    TntLabel25: TTntLabel;
    TntLabel26: TTntLabel;
    TntShape8: TTntShape;
    TntLabel27: TTntLabel;
    TntShape9: TTntShape;
    TntLabel28: TTntLabel;
    TntShape10: TTntShape;
    TntShape12: TTntShape;
    TntShape14: TTntShape;
    TntLabel36: TTntLabel;
    MonthHM3: TTntComboBox;
    AmarMotAndMash1: TTntRadioButton;
    AmarMotAndMash2: TTntRadioButton;
    AmarMotAndMash3: TTntRadioButton;
    AmarMotAndMash4: TTntRadioButton;
    AmarMotAndMash5: TTntRadioButton;
    TntPanel1: TTntPanel;
    TntButton8: TTntButton;
    AjansCB3: TTntComboBox;
    TntLabel9: TTntLabel;
    YearCB3: TTntComboBox;
    HM1: TTntRadioButton;
    HM2: TTntRadioButton;
    HM3: TTntRadioButton;
    HM4: TTntRadioButton;
    TntLabel11: TTntLabel;
    DayCB1: TTntComboBox;
    TntLabel20: TTntLabel;
    TntLabel21: TTntLabel;
    MonthCB1: TTntComboBox;
    YearED1: TTntEdit;
    TanzimTarikh: TTntButton;
    TntLabel29: TTntLabel;
    AddAnbarTxt: TTntEdit;
    TntButton16: TTntButton;
    TntLabel38: TTntLabel;
    TntLabel40: TTntLabel;
    AnbarCB1: TTntComboBox;
    TntLabel42: TTntLabel;
    TntLabel43: TTntLabel;
    YeganCB1: TTntComboBox;
    AddYeganTxt: TTntEdit;
    TntLabel44: TTntLabel;
    TntButton17: TTntButton;
    TntLabel45: TTntLabel;
    AddAjansTxt: TTntEdit;
    TntLabel46: TTntLabel;
    TntButton19: TTntButton;
    AjansCB1: TTntComboBox;
    TntLabel47: TTntLabel;
    TntButton20: TTntButton;
    TntButton21: TTntButton;
    TntButton22: TTntButton;
    TntButton23: TTntButton;
    TntLabel48: TTntLabel;
    YearCB1: TTntComboBox;
    TntButton25: TTntButton;
    DayHM1: TTntComboBox;
    TntLabel10: TTntLabel;
    MonthHM1: TTntComboBox;
    TntLabel16: TTntLabel;
    TntButton18: TTntButton;
    TntButton26: TTntButton;
    TntButton27: TTntButton;
    TntLabel19: TTntLabel;
    SearchAjans: TTntEdit;
    TntLabel30: TTntLabel;
    TntButton30: TTntButton;
    TntButton12: TTntButton;
    TntButton28: TTntButton;
    TntButton9: TTntButton;
    TntDBGrid3: TTntDBGrid;
    MashinDS: TDataSource;
    AjansDS: TDataSource;
    AllAjansPrn: TRvProject;
    AllMashinPrn: TRvProject;
    AllMotorPrn: TRvProject;
    AllAjansPrnDSCon: TRvDataSetConnection;
    AllMashinPrnDSCon: TRvDataSetConnection;
    AllMotorPrnDSCon: TRvDataSetConnection;
    MotorDS: TDataSource;
    MotorGozPrn: TRvProject;
    MashinGozPrn: TRvProject;
    AjansGozPrn: TRvProject;
    MotorGozPrnDSCon: TRvDataSetConnection;
    MashinGozPrnDSCon: TRvDataSetConnection;
    AjansGozPrnDSCon: TRvDataSetConnection;
    TntButton13: TTntButton;
    TntButton29: TTntButton;
    TntShape11: TTntShape;
    TntLabel31: TTntLabel;
    AddNooZabt: TTntEdit;
    TntLabel32: TTntLabel;
    TntLabel33: TTntLabel;
    NooZabtCB6: TTntComboBox;
    TntLabel34: TTntLabel;
    TntButton31: TTntButton;
    TntButton32: TTntButton;
    TntButton33: TTntButton;
    TntButton34: TTntButton;
    TntButton35: TTntButton;
    TntButton36: TTntButton;
    TntLabel13: TTntLabel;
    TntShape13: TTntShape;
    TntButton37: TTntButton;
    TntButton38: TTntButton;
    TntLabel14: TTntLabel;
    AddMamoreBargeZard: TTntEdit;
    TntLabel35: TTntLabel;
    TntLabel37: TTntLabel;
    MamoreBargeZardCB1: TTntComboBox;
    TntLabel39: TTntLabel;
    TntShape15: TTntShape;
    TntLabel41: TTntLabel;
    TntLabel49: TTntLabel;
    MotRecCount: TTntLabel;
    MashRecCount: TTntLabel;
    TntLabel51: TTntLabel;
    DayHM2: TTntComboBox;
    MonthHM2: TTntComboBox;
    TntLabel50: TTntLabel;
    Pelak1: TTntComboBox;
    Pelak2: TTntComboBox;
    Pelak3: TTntComboBox;
    Pelak4: TTntComboBox;
    AmarMotAndMash6: TTntRadioButton;
    TntSpeedButton1: TTntSpeedButton;
    TahvileBargeZard: TPanel;
    TntShape16: TTntShape;
    TntShape17: TTntShape;
    TntDBGrid4: TTntDBGrid;
    TntButton24: TTntButton;
    TntButton39: TTntButton;
    TntButton40: TTntButton;
    TntButton41: TTntButton;
    TntButton42: TTntButton;
    TntButton43: TTntButton;
    TntLabel52: TTntLabel;
    BargeZardSearch: TTntEdit;
    TntLabel53: TTntLabel;
    TntLabel54: TTntLabel;
    TntLabel55: TTntLabel;
    TntButton44: TTntButton;
    MamoreBargeZardCB2: TTntComboBox;
    DayTBZ1: TTntComboBox;
    MonTBZ1: TTntComboBox;
    YearCB4: TTntComboBox;
    TntLabel58: TTntLabel;
    TntLabel59: TTntLabel;
    MonTBZ2: TTntComboBox;
    TBZ3: TTntRadioButton;
    TBZ2: TTntRadioButton;
    TntLabel56: TTntLabel;
    TBZ1: TTntRadioButton;
    TntButton45: TTntButton;
    TahvileBaregZardDS: TDataSource;
    AllTahvileBaregZardPrn: TRvProject;
    AllTahvileBaregZardPrnDSCon: TRvDataSetConnection;
    TahvileBaregZardGozPrn: TRvProject;
    AllTahvileBaregZardPrnGozDSCon: TRvDataSetConnection;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image8: TImage;
    Image9: TImage;
    Image10: TImage;
    Image11: TImage;
    Image12: TImage;
    LMDGlobalHotKey1: TLMDGlobalHotKey;
    LMDGlobalHotKey2: TLMDGlobalHotKey;
    Image6: TImage;
    Image7: TImage;
    TntShape18: TTntShape;
    TntButton46: TTntButton;
    MR2: TTntRadioButton;
    MR1: TTntRadioButton;
    MRMonth: TTntComboBox;
    YearCB5: TTntComboBox;
    TntLabel61: TTntLabel;
    TntLabel62: TTntLabel;
    Panel1: TPanel;
    TntButton15: TTntButton;
    TntLabel24: TTntLabel;
    MonthPercentBut2: TTntComboBox;
    HP2: TTntRadioButton;
    HP1: TTntRadioButton;
    MonthPercentBut1: TTntComboBox;
    TntLabel23: TTntLabel;
    HazineMonth: TTntComboBox;
    TntShape3: TTntShape;
    YearCB2: TTntComboBox;
    TntLabel7: TTntLabel;
    TntLabel18: TTntLabel;
    AjansGozDBTable: TTable;
    AjansGozDBTableAjansName: TStringField;
    AjansGozDBTableTarikh: TStringField;
    AjansGozDBTableTedadeMotor: TSmallintField;
    AjansGozDBTableYegan: TStringField;
    AjansGozDBTableMablageDaryafty: TIntegerField;
    AjansDBTable: TTable;
    AjansDBTableAjansName: TStringField;
    AjansDBTableTarikh: TStringField;
    AjansDBTableTedadeMotor: TSmallintField;
    AjansDBTableYegan: TStringField;
    AjansDBTableMablageDaryafty: TIntegerField;
    MashinGozDBTable: TTable;
    MashinGozDBTableCodeBrgeZard: TStringField;
    MashinGozDBTableNoo: TStringField;
    MashinGozDBTableRang: TStringField;
    MashinGozDBTableTarikhVorod: TStringField;
    MashinGozDBTableShomarePelak: TStringField;
    MashinGozDBTableShomareBadane: TStringField;
    MashinGozDBTableShomareMotor: TStringField;
    MashinGozDBTableYegan: TStringField;
    MashinGozDBTableAnbar: TStringField;
    MashinGozDBTableRadif: TSmallintField;
    MashinGozDBTableTarikhTarkhis: TStringField;
    MashinGozDBTableHazineDaryafti: TFloatField;
    MashinGozDBTableNooeZabt: TStringField;
    MashinGozDBTableVSSwitch: TBooleanField;
    MashinGozDBTableVSFandak: TBooleanField;
    MashinGozDBTableVSJak: TBooleanField;
    MashinGozDBTableVSAchaCharkh: TBooleanField;
    MashinGozDBTableVSAinehDakhel: TBooleanField;
    MashinGozDBTableVSZapas: TBooleanField;
    MashinGozDBTableVSAinehBagal: TBooleanField;
    MashinGozDBTableVSGalpagh: TBooleanField;
    MashinGozDBTableVSRadioPakhsh: TBooleanField;
    MashinGozDBTableVSProjektor: TBooleanField;
    MashinGozDBTableVSBolandgoo: TBooleanField;
    MashinGozDBTableVSCapsol: TBooleanField;
    MashinGozDBTableVSGoflVaZanjir: TBooleanField;
    MashinGozDBTableTozihat: TStringField;
    MashinDBTable: TTable;
    MashinDBTableCodeBrgeZard: TStringField;
    MashinDBTableNoo: TStringField;
    MashinDBTableRang: TStringField;
    MashinDBTableTarikhVorod: TStringField;
    MashinDBTableShomarePelak: TStringField;
    MashinDBTableShomareBadane: TStringField;
    MashinDBTableShomareMotor: TStringField;
    MashinDBTableYegan: TStringField;
    MashinDBTableAnbar: TStringField;
    MashinDBTableRadif: TSmallintField;
    MashinDBTableTarikhTarkhis: TStringField;
    MashinDBTableHazineDaryafti: TFloatField;
    MashinDBTableNooeZabt: TStringField;
    MashinDBTableVSSwitch: TBooleanField;
    MashinDBTableVSFandak: TBooleanField;
    MashinDBTableVSJak: TBooleanField;
    MashinDBTableVSAchaCharkh: TBooleanField;
    MashinDBTableVSAinehDakhel: TBooleanField;
    MashinDBTableVSZapas: TBooleanField;
    MashinDBTableVSAinehBagal: TBooleanField;
    MashinDBTableVSGalpagh: TBooleanField;
    MashinDBTableVSRadioPakhsh: TBooleanField;
    MashinDBTableVSProjektor: TBooleanField;
    MashinDBTableVSBolandgoo: TBooleanField;
    MashinDBTableVSCapsol: TBooleanField;
    MashinDBTableVSGoflVaZanjir: TBooleanField;
    MashinDBTableTozihat: TStringField;
    MotorGozDBTable: TTable;
    MotorGozDBTableCodeBrgeZard: TStringField;
    MotorGozDBTableNoo: TStringField;
    MotorGozDBTableRang: TStringField;
    MotorGozDBTableTarikhVorod: TStringField;
    MotorGozDBTableShomarePelak: TStringField;
    MotorGozDBTableShomareBadane: TStringField;
    MotorGozDBTableShomareMotor: TStringField;
    MotorGozDBTableYegan: TStringField;
    MotorGozDBTableAnbar: TStringField;
    MotorGozDBTableRadif: TSmallintField;
    MotorGozDBTableTarikhTarkhis: TStringField;
    MotorGozDBTableHazineDaryafti: TFloatField;
    MotorGozDBTableNooeZabt: TStringField;
    MotorGozDBTableVSJak: TBooleanField;
    MotorGozDBTableVSGabZanjir: TBooleanField;
    MotorGozDBTableVSRecabJelo: TBooleanField;
    MotorGozDBTableVSHendel: TBooleanField;
    MotorGozDBTableVSAchar: TBooleanField;
    MotorGozDBTableVSBatri: TBooleanField;
    MotorGozDBTableVSBaak: TBooleanField;
    MotorGozDBTableVSGabBagalDotarf: TBooleanField;
    MotorGozDBTableVSDasteTormoz: TBooleanField;
    MotorGozDBTableVSDasteKelaj: TBooleanField;
    MotorGozDBTableVSKaseKilometr: TBooleanField;
    MotorGozDBTableVSKaseDoreMotor: TBooleanField;
    MotorGozDBTableVSCherageJelo: TBooleanField;
    MotorGozDBTableVSCherageAgab: TBooleanField;
    MotorGozDBTableVSCherageRahnamaChap: TBooleanField;
    MotorGozDBTableVSCherageRahnamaRast: TBooleanField;
    MotorGozDBTableVSAineh: TBooleanField;
    MotorGozDBTableVSToshak: TBooleanField;
    MotorGozDBTableVSArmeBadaneh: TBooleanField;
    MotorGozDBTableVSBoog: TBooleanField;
    MotorGozDBTableVSGofVaZanjir: TBooleanField;
    MotorGozDBTableVSTalg: TBooleanField;
    MotorGozDBTableVSSwitch: TBooleanField;
    MotorGozDBTableTozihat: TStringField;
    MotorDBTable: TTable;
    MotorDBTableCodeBrgeZard: TStringField;
    MotorDBTableNoo: TStringField;
    MotorDBTableRang: TStringField;
    MotorDBTableTarikhVorod: TStringField;
    MotorDBTableShomarePelak: TStringField;
    MotorDBTableShomareBadane: TStringField;
    MotorDBTableShomareMotor: TStringField;
    MotorDBTableYegan: TStringField;
    MotorDBTableAnbar: TStringField;
    MotorDBTableRadif: TSmallintField;
    MotorDBTableTarikhTarkhis: TStringField;
    MotorDBTableHazineDaryafti: TFloatField;
    MotorDBTableNooeZabt: TStringField;
    MotorDBTableVSJak: TBooleanField;
    MotorDBTableVSGabZanjir: TBooleanField;
    MotorDBTableVSRecabJelo: TBooleanField;
    MotorDBTableVSHendel: TBooleanField;
    MotorDBTableVSAchar: TBooleanField;
    MotorDBTableVSBatri: TBooleanField;
    MotorDBTableVSBaak: TBooleanField;
    MotorDBTableVSGabBagalDotarf: TBooleanField;
    MotorDBTableVSDasteTormoz: TBooleanField;
    MotorDBTableVSDasteKelaj: TBooleanField;
    MotorDBTableVSKaseKilometr: TBooleanField;
    MotorDBTableVSKaseDoreMotor: TBooleanField;
    MotorDBTableVSCherageJelo: TBooleanField;
    MotorDBTableVSCherageAgab: TBooleanField;
    MotorDBTableVSCherageRahnamaChap: TBooleanField;
    MotorDBTableVSCherageRahnamaRast: TBooleanField;
    MotorDBTableVSAineh: TBooleanField;
    MotorDBTableVSToshak: TBooleanField;
    MotorDBTableVSArmeBadaneh: TBooleanField;
    MotorDBTableVSBoog: TBooleanField;
    MotorDBTableVSGofVaZanjir: TBooleanField;
    MotorDBTableVSTalg: TBooleanField;
    MotorDBTableVSSwitch: TBooleanField;
    MotorDBTableTozihat: TStringField;
    TahvileBaregZardDB: TTable;
    TahvileBaregZardDBCodeBrgeZard: TStringField;
    TahvileBaregZardDBTarikhVorod: TStringField;
    TahvileBaregZardDBShomarePelak: TStringField;
    TahvileBaregZardDBShomareBadane: TStringField;
    TahvileBaregZardDBShomareMotor: TStringField;
    TahvileBaregZardDBNooeZabt: TStringField;
    TahvileBaregZardDBNameAfsar: TStringField;
    TahvileBaregZardDBTarikheTahvil: TStringField;
    TahvileBaregZardDBNooeVasileh: TStringField;
    TahvileBaregZardGozDBTable: TTable;
    TahvileBaregZardGozDBTableCodeBrgeZard: TStringField;
    TahvileBaregZardGozDBTableTarikhVorod: TStringField;
    TahvileBaregZardGozDBTableShomarePelak: TStringField;
    TahvileBaregZardGozDBTableShomareBadane: TStringField;
    TahvileBaregZardGozDBTableShomareMotor: TStringField;
    TahvileBaregZardGozDBTableNooeZabt: TStringField;
    TahvileBaregZardGozDBTableNameAfsar: TStringField;
    TahvileBaregZardGozDBTableTarikheTahvil: TStringField;
    TahvileBaregZardGozDBTableNooeVasileh: TStringField;
    procedure InOutButClick(Sender: TObject);
    procedure GozButClick(Sender: TObject);
    procedure AjButClick(Sender: TObject);
    procedure TanButClick(Sender: TObject);
    function ChkPass(Msg:string):boolean;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TntButton20Click(Sender: TObject);
    procedure TntButton21Click(Sender: TObject);
    procedure TntSpeedButton1Click(Sender: TObject);
    procedure TntSpeedButton2Click(Sender: TObject);

    procedure AddCBs(CBTyp,Meghdar:widestring);
    procedure RemoveFromCBs(const CBTyp,Meghdar:widestring);
    procedure FileToCBs(CBTyp,FileName:widestring);
    procedure CBsToFile(CBTyp,FileName:widestring);
    function ChkExistInCBs(CBTyp,Meghdar:widestring):boolean;
    function RetNoInCBs(CBTyp,Meghdar:widestring):integer;

    procedure TntButton16Click(Sender: TObject);
    procedure TntButton17Click(Sender: TObject);
    procedure TntButton19Click(Sender: TObject);
    procedure TntButton18Click(Sender: TObject);
    procedure TntButton26Click(Sender: TObject);
    procedure TntButton27Click(Sender: TObject);
    procedure TanzimTarikhClick(Sender: TObject);
    procedure YearED1KeyPress(Sender: TObject; var Key: Char);
    procedure MonthPercentBut1KeyPress(Sender: TObject; var Key: Char);
    procedure TntButton9Click(Sender: TObject);
    procedure newbutClick(Sender: TObject);
    procedure TntButton1Click(Sender: TObject);
    procedure TntButton4Click(Sender: TObject);
    procedure TntButton5Click(Sender: TObject);
    procedure TntButton7Click(Sender: TObject);
    procedure TntButton28Click(Sender: TObject);
    procedure TntButton12Click(Sender: TObject);
    procedure TntButton11Click(Sender: TObject);
    procedure TntButton2Click(Sender: TObject);
    procedure TntButton22Click(Sender: TObject);
    procedure TntButton23Click(Sender: TObject);
    procedure TntButton13Click(Sender: TObject);
    procedure TntButton29Click(Sender: TObject);
    procedure TntButton10Click(Sender: TObject);
    procedure TntButton3Click(Sender: TObject);
    procedure TntButton32Click(Sender: TObject);
    procedure TntButton31Click(Sender: TObject);
    procedure TntButton33Click(Sender: TObject);
    procedure TntButton34Click(Sender: TObject);
    procedure TntButton30Click(Sender: TObject);
    procedure TntButton36Click(Sender: TObject);
    procedure TntButton35Click(Sender: TObject);
    procedure TntButton6Click(Sender: TObject);
    procedure TntButton15Click(Sender: TObject);
    procedure MonthPercentBut2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TntButton14Click(Sender: TObject);
    procedure MotorDBTableAfterPost(DataSet: TDataSet);
    procedure MotorDBTableAfterOpen(DataSet: TDataSet);
    procedure MotorDBTableAfterRefresh(DataSet: TDataSet);
    procedure MashinDBTableAfterOpen(DataSet: TDataSet);
    procedure MashinDBTableAfterPost(DataSet: TDataSet);
    procedure MashinDBTableAfterRefresh(DataSet: TDataSet);
    procedure TntButton8Click(Sender: TObject);
    procedure TntButton43Click(Sender: TObject);
    procedure TntButton24Click(Sender: TObject);
    procedure TntButton39Click(Sender: TObject);
    procedure TntButton41Click(Sender: TObject);
    procedure TntButton42Click(Sender: TObject);
    procedure TntButton40Click(Sender: TObject);
    procedure TntButton38Click(Sender: TObject);
    procedure TntButton37Click(Sender: TObject);
    procedure TntButton25Click(Sender: TObject);
    procedure TntButton44Click(Sender: TObject);
    procedure LMDGlobalHotKey1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LMDGlobalHotKey2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SearchBCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SearchBCodeMashinKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TntButton46Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    function FirstRunPass():boolean;
  public
    AnbarNameFileAdd:string;
    AjansNameFileAdd:string;
    YeganNameFileAdd:string;
    YearNameFileAdd:string;
    AddNooZabtFileAdd:string;
    MamoreBargeZardFileAdd:string;
    function Mmessagebox(Msg,Cap:string;Typ:longint):integer;
  end;

var
  MainF: TMainF;

implementation

uses Unit3, Unit4, Unit5, Unit7, Unit9, Unit10, Unit2, Unit8;

{$R *.dfm}


procedure TMainF.InOutButClick(Sender: TObject);
begin
Reports.Visible:=false;
Ajans.Visible:=false;
Tanzimat.Visible:=false;
TahvileBargeZard.Visible:=false;
InputOutput.Visible:=true;
end;

procedure TMainF.GozButClick(Sender: TObject);
begin
TahvileBargeZard.Visible:=false;
InputOutput.Visible:=false;
Ajans.Visible:=false;
Tanzimat.Visible:=false;
Reports.Visible:=true;
end;

procedure TMainF.AjButClick(Sender: TObject);
begin
TahvileBargeZard.Visible:=false;
InputOutput.Visible:=false;
Tanzimat.Visible:=false;
Reports.Visible:=false;
Ajans.Visible:=true;
end;

procedure TMainF.TanButClick(Sender: TObject);
begin
TahvileBargeZard.Visible:=false;
InputOutput.Visible:=false;
Reports.Visible:=false;
Ajans.Visible:=false;
Tanzimat.Visible:=true;


DayCB1.ItemIndex:=TFarDate.fardayOf(TFarDate.MiladyToShamsi(now))-1;
MonthCB1.ItemIndex:=TFarDate.farMonthOf(TFarDate.MiladyToShamsi(now))-1;
YearED1.Text:=inttostr(TFarDate.farYearOf(TFarDate.MiladyToShamsi(now)));
end;

function TMainF.ChkPass(Msg: string): boolean;
var
TmpReg:TRegistry;
PassMD5:string;
begin
TmpReg:=Tregistry.Create;
PassMD5:='NotPass';
  try
    TmpReg.RootKey := HKEY_LOCAL_MACHINE;
    TmpReg.OpenKey('SOFTWARE\IVS\MN\Parking', False);
    PassMD5 := TmpReg.ReadString('Addr');
  finally
    TmpReg.Free;
  end;

if (PassMD5='NotPass')or(PassMD5='') then
  if not(FirstRunPass) then begin
    result:=false;
    exit;
    end
  else begin
    result:=true;
    exit;
    end;

ChkPassF.MsgL.Lines.Clear;
ChkPassF.MsgL.Lines.Add(Msg);
result:=false;
if ChkPassF.ShowModal=MrCancel then exit;
if MD5Print(MD5String(ChkPassF.Pass))=PassMD5 then begin
  result:=true;
  exit;
  end;
result:=false;
end;

function TMainF.FirstRunPass():boolean;
var
TmpReg:Tregistry;
label mpr;
begin
if Mmessagebox('��� ����� ��� ��� �� ��� ��� ������ �� ���� �� ����'+chr(13)+
  '� �� ����� �� ���� ��� ��� ���� ����� ��� ���� � Ӂ� ��� ���� �� ���� ������'
  ,'��� �� ������ ���',MB_OKCANCEL)=IDCancel then begin
      result:=false;
      application.Terminate;
      exit;
      end;
mpr:
if MainPassF.ShowModal=MRCancel then begin
      result:=false;
      exit;
      end;
//dsFGeh23dfcaDC415
if MD5Print(MD5String(MainPassF.MainPass.Text))<>'a9850eaf3e939733f1cc368dc14133e4' then begin
      showmessage('��� ���� ������ ��ʡ ����� ���� ���� �� ����� ����');
      goto mpr;
      end;
ChkPassF.MsgL.Lines.Clear;
ChkPassF.MsgL.Lines.Add('����� ��� ����.');
if (ChkPassF.ShowModal=MRCancel)or(ChkPassF.Pass='') then begin
      showmessage('���� ����� ��ϡ ����� ���� ����.');
      result:=false;
      exit;
      end;

TmpReg:=Tregistry.Create;
  try
    TmpReg.RootKey:= HKEY_LOCAL_MACHINE;
    TmpReg.OpenKey('SOFTWARE\IVS\MN\Parking', true);
    TmpReg.WriteString('Addr',MD5Print(MD5String(ChkPassF.Pass)));
  finally
    TmpReg.Free;
  end;
Result:=true;
end;

function TMainF.Mmessagebox(Msg, Cap: string; Typ: Integer): integer;
var
a:integer;
begin
MMessageF.Caption:=Cap;
MMessageF.MMsgText.Caption:=Msg;
Result:=0;
if Typ=MB_YesNo then begin
    MMessageF.Btn1.Visible:=true;
    MMessageF.Btn1.Caption:='���';
    MMessageF.Btn2.Visible:=true;
    MMessageF.Btn2.Caption:='���';
    result:=IDNO;
    if MMessageF.ShowModal=MRYES then result:=IDYES;
  end
  else if Typ=MB_OkCancel then begin
    MMessageF.Btn1.Visible:=true;
    MMessageF.Btn1.Caption:='����� ���';
    MMessageF.Btn2.Visible:=true;
    MMessageF.Btn2.Caption:='���';
    result:=IDOK;
    if MMessageF.ShowModal=MRNO then result:=IDCANCEL;
  end
  else if Typ=MB_Ok then begin
    a:=MMessageF.Btn1.Left;
    MMessageF.Btn1.Left:=(MMessageF.Width div 2)-(MMessageF.Btn1.Width div 2);
    MMessageF.Btn1.Visible:=true;
    MMessageF.Btn1.Caption:='����� ���';
    MMessageF.Btn2.Visible:=false;
    if MMessageF.ShowModal=MRYES then result:=IDOK;
    MMessageF.Btn1.Left:=a;
  end;
end;

procedure TMainF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
MotorDBTable.Close;
MotorGozDBTable.Close;
MashinDBTable.Close;
MashinGozDBTable.Close;
AjansDBTable.Close;
AjansGozDBTable.Close;
TahvileBaregZardDB.Close;
TahvileBaregZardGozDBTable.Close;

application.Terminate;
end;

procedure TMainF.TntButton20Click(Sender: TObject);
var
TmpReg:Tregistry;
begin
MMessageBox('���� ����� ��� ����� ���� ��� ����� �� ���� ���ϡ Ӂ� ��� ���� �� �� ��� ������ �� ���.','����� ���',MB_OK);
if not ChkPass('���� ����� ��ҡ ��� ����� �� ���� ������') then begin
  MMessageBox('��� ������','��� ����� ������ ���� �ϡ ������ ���� ��� ���',MB_OK);
  exit;
  end;

ChkPassF.MsgL.Lines.Clear;
ChkPassF.MsgL.Lines.Add('����� ��� ����.');
if (ChkPassF.ShowModal=MRCancel)or(ChkPassF.Pass='') then begin
      showmessage('���� ����� ��ϡ ����� ���� ����.');
      exit;
      end;

TmpReg:=Tregistry.Create;
  try
    TmpReg.RootKey:= HKEY_LOCAL_MACHINE;
    TmpReg.OpenKey('SOFTWARE\IVS\MN\Parking', true);
    TmpReg.WriteString('Addr',MD5Print(MD5String(ChkPassF.Pass)));
  finally
    TmpReg.Free;
  end;
end;

procedure TMainF.TntButton21Click(Sender: TObject);
var TmpReg:Tregistry;
begin
if MMessageBox('�����!'+chr(13)+'ǐ� ��� �� ��� ���� �� ����� ���� ������ ���� �� ��� ���� �������� ���� ������ ����. ����� �� ���Ͽ','��� ���',MB_YESNO)=IDYES then begin
TmpReg:=Tregistry.Create;
    try
      TmpReg.RootKey:= HKEY_LOCAL_MACHINE;
      TmpReg.OpenKey('SOFTWARE\IVS\MN\Parking', true);
      TmpReg.DeleteValue('Addr');
    finally
      TmpReg.Free;
    end;
  end;
end;

procedure TMainF.TntSpeedButton1Click(Sender: TObject);
begin
TahvileBargeZard.Visible:=true;
InputOutput.Visible:=false;
Tanzimat.Visible:=false;
Reports.Visible:=false;
Ajans.Visible:=false;

end;

procedure TMainF.TntSpeedButton2Click(Sender: TObject);
begin
About.ShowModal;
end;

procedure TMainF.AddCBs(CBTyp, Meghdar: widestring);
begin
if CBTyp='NooZabtCB' then begin
  NooZabtCB1.Items.Add(Meghdar);
  NooZabtCB2.Items.Add(Meghdar);
  NooZabtCB3.Items.Add(Meghdar);
  NooZabtCB4.Items.Add(Meghdar);
  NooZabtCB6.Items.Add(Meghdar);

  NewMotAndMash.NooZabtCB5.Items.Add(Meghdar);
end

else if CBTyp='MamoreBargeZard' then begin
  MamoreBargeZardCB1.Items.Add(Meghdar);
  MamoreBargeZardCB2.Items.Add(Meghdar);
  TahvilBargeZardF.MamoreBargeZardCB3.Items.Add(Meghdar);
end

else if CBTyp='AjansCB' then begin
    with MainF do begin
        AjansCB1.Items.Add(Meghdar);
        AjansCB3.Items.Add(Meghdar);
    end;
    NewMotorOfAjans.AjansCB2.Items.Add(Meghdar);
end

else if CBTyp='YearCB' then begin
    with MainF do begin
        YearCB1.Items.Add(Meghdar);
        YearCB2.Items.Add(Meghdar);
        YearCB3.Items.Add(Meghdar);
        YearCB4.Items.Add(Meghdar);
        YearCB5.Items.Add(Meghdar);
        end;
    end

else if CBTyp='YeganCB' then begin
    with MainF do begin
      YeganCB1.Items.Add(Meghdar);
      YeganCB2.Items.Add(Meghdar);
    end;
    NewMotorOfAjans.YeganCB3.Items.Add(Meghdar);
    NewMotAndMash.YeganCB1.Items.Add(Meghdar);
end

else if CBTyp='AnbarCB' then begin
    with MainF do begin
        AnbarCB1.Items.Add(Meghdar);
        AnbarCB2.Items.Add(Meghdar);
        end;
    NewMotAndMash.AnbarCB1.Items.Add(Meghdar);
    end

else begin
    Mmessagebox('��� �� ��� �� ��','AddCBS',MB_OK);
    end;
end;

procedure TMainF.FileToCBs(CBTyp, FileName: widestring);
begin
if not(fileexists(FileName)) then begin
    Mmessagebox('��� �� ��� ����','FileToCBs',MB_OK);
    exit;
end;

if CBTyp='NooZabtCB' then begin
  NooZabtCB1.Items.LoadFromFile(FileName);
  NooZabtCB2.Items.LoadFromFile(FileName);
  NooZabtCB3.Items.LoadFromFile(FileName);
  NooZabtCB4.Items.LoadFromFile(FileName);
  NooZabtCB6.Items.LoadFromFile(FileName);
  NewMotAndMash.NooZabtCB5.Items.LoadFromFile(FileName);
end

else if CBTyp='MamoreBargeZard' then begin
  MamoreBargeZardCB1.Items.LoadFromFile(FileName);
  MamoreBargeZardCB2.Items.LoadFromFile(FileName);
  TahvilBargeZardF.MamoreBargeZardCB3.Items.LoadFromFile(FileName);
end

else if CBTyp='AjansCB' then begin
  AjansCB1.Items.LoadFromFile(FileName);
  AjansCB3.Items.LoadFromFile(FileName);
  NewMotorOfAjans.AjansCB2.Items.LoadFromFile(FileName);
end

else if CBTyp='YearCB' then begin
  with MainF do begin
    YearCB1.Items.LoadFromFile(FileName);
    YearCB2.Items.LoadFromFile(FileName);
    YearCB3.Items.LoadFromFile(FileName);
    YearCB4.Items.LoadFromFile(FileName);
    YearCB5.Items.LoadFromFile(FileName);
  end;
end

else if CBTyp='YeganCB' then begin
  with MainF do begin
    YeganCB1.Items.LoadFromFile(FileName);
    YeganCB2.Items.LoadFromFile(FileName);
  end;
  NewMotorOfAjans.YeganCB3.Items.LoadFromFile(FileName);
  NewMotAndMash.YeganCB1.Items.LoadFromFile(FileName);
end

else if CBTyp='AnbarCB' then begin
  with MainF do begin
    AnbarCB1.Items.LoadFromFile(FileName);
    AnbarCB2.Items.LoadFromFile(FileName);
  end;
  NewMotAndMash.AnbarCB1.Items.LoadFromFile(FileName);
end

else begin
    Mmessagebox('��� �� ��� �� ��','FileToCBs',MB_OK);
end;
end;

function TMainF.ChkExistInCBs(CBTyp, Meghdar: widestring): boolean;
var
i:integer;
begin
result:=false;

if CBTyp='NooZabtCB' then begin
  for i:=0 to NooZabtCB1.Items.Count-1 do begin
    if NooZabtCB1.Items[i]=Meghdar then begin
      result:=true;
      exit;
    end;
  end;
end

else if CBTyp='MamoreBargeZard' then begin
  for i:=0 to MamoreBargeZardCB1.Items.Count-1 do begin
    if MamoreBargeZardCB1.Items[i]=Meghdar then begin
      result:=true;
      exit;
    end;
  end;
end

else if CBTyp='AjansCB' then begin
  for i:=0 to AjansCB1.Items.Count-1 do begin
    if AjansCB1.Items[i]=Meghdar then begin
      result:=true;
      exit;
    end;
  end;
end

else if CBTyp='YearCB' then begin
  for i:=0 to YearCB1.Items.Count-1 do begin
    if YearCB1.Items[i]=Meghdar then begin
      result:=true;
      exit;
    end;
  end;
end

else if CBTyp='YeganCB' then begin
  for i:=0 to YeganCB1.Items.Count-1 do begin
    if YeganCB1.Items[i]=Meghdar then begin
      result:=true;
      exit;
    end;
  end;
end

else if CBTyp='AnbarCB' then begin
  for i:=0 to AnbarCB1.Items.Count-1 do begin
    if AnbarCB1.Items[i]=Meghdar then begin
      result:=true;
      exit;
    end;
  end;
end

else begin
  Mmessagebox('��� �� ��� �� ��','ChkExistInCBs',MB_OK);
end;
end;

procedure TMainF.TntButton16Click(Sender: TObject);
begin
if AddAnbarTxt.text ='' then begin
  MMessageBox('��� ����� ���� ����� ���� ���.','���',MB_OK);
  exit;
end;
AddCBs('AnbarCB',AddAnbarTxt.text);
AnbarCB1.Items.SaveToFile(AnbarNameFileAdd);
AddAnbarTxt.text:='';
end;

procedure TMainF.CBsToFile(CBTyp, FileName: widestring);
begin
if CBTyp='NooZabtCB' then begin
  NooZabtCB1.Items.SaveToFile(FileName);
end

else if CBTyp='MamoreBargeZard' then begin
  MamoreBargeZardCB1.Items.SaveToFile(FileName);
end

else if CBTyp='AjansCB' then begin
  AjansCB1.Items.SaveToFile(FileName);
end

else if CBTyp='YearCB' then begin
  YearCB1.Items.SaveToFile(FileName);
end

else if CBTyp='YeganCB' then begin
  YeganCB1.Items.SaveToFile(FileName);
end

else if CBTyp='AnbarCB' then begin
  AnbarCB1.Items.SaveToFile(FileName);
end

else begin
    Mmessagebox('��� �� ��� �� ��','CBsToFile',MB_OK);
end;
end;

procedure TMainF.RemoveFromCBs(const CBTyp, Meghdar: widestring);
var
i:integer;
begin

if CBTyp='NooZabtCB' then begin
  for i:=0 to NooZabtCB1.Items.Count-1 do begin
    if NooZabtCB1.Items[i]=Meghdar then begin
      NooZabtCB1.Items.Delete(i);
      NooZabtCB2.Items.Delete(i);
      NooZabtCB3.Items.Delete(i);
      NooZabtCB4.Items.Delete(i);
      NooZabtCB6.Items.Delete(i);
      NewMotAndMash.NooZabtCB5.Items.Delete(i);
      exit;
    end;
  end;
end

else if CBTyp='MamoreBargeZard' then begin
  for i:=0 to MamoreBargeZardCB1.Items.Count-1 do begin
    if MamoreBargeZardCB1.Items[i]=Meghdar then begin
      MamoreBargeZardCB1.Items.Delete(i);
      MamoreBargeZardCB2.Items.Delete(i);
      TahvilBargeZardF.MamoreBargeZardCB3.Items.Delete(i);
    end;
  end;
end


else if CBTyp='AjansCB' then begin
  for i:=0 to AjansCB1.Items.Count-1 do begin
    if AjansCB1.Items[i]=Meghdar then begin
      with MainF do begin
        AjansCB1.Items.Delete(i);
        AjansCB3.Items.Delete(i);
      end;
      NewMotorOfAjans.AjansCB2.Items.Delete(i);

      exit;
    end;
  end;
end

else if CBTyp='YearCB' then begin
  for i:=0 to YearCB1.Items.Count-1 do begin
    if YearCB1.Items[i]=Meghdar then begin
      with MainF do begin
        YearCB1.Items.Delete(i);
        YearCB2.Items.Delete(i);
        YearCB3.Items.Delete(i);
        YearCB4.Items.Delete(i);
        YearCB5.Items.Delete(i);
      end;

      exit;
    end;
  end;
end

else if CBTyp='YeganCB' then begin
  for i:=0 to YeganCB1.Items.Count-1 do begin
    if YeganCB1.Items[i]=Meghdar then begin
      with MainF do begin
        YeganCB1.Items.Delete(i);
        YeganCB2.Items.Delete(i);
      end;
      NewMotorOfAjans.YeganCB3.Items.Delete(i);
      NewMotAndMash.YeganCB1.Items.Add(Meghdar);

      exit;
    end;
  end;
end

else if CBTyp='AnbarCB' then begin
  for i:=0 to AnbarCB1.Items.Count-1 do begin
    if AnbarCB1.Items[i]=Meghdar then begin
      with MainF do begin
        AnbarCB1.Items.Delete(i);
        AnbarCB2.Items.Delete(i);
      end;
      NewMotAndMash.AnbarCB1.Items.Add(Meghdar);

      exit;
    end;
  end;
end

else begin
  Mmessagebox('��� �� ��� �� ��','RemoveFromCBs',MB_OK);
end;
end;

procedure TMainF.TntButton17Click(Sender: TObject);
begin
if AddYeganTxt.Text ='' then begin
  MMessageBox('��� ����� ���� ���� ������ ���� ���.','���',MB_OK);
  exit;
end;
AddCBs('YeganCB',AddYeganTxt.Text);
YeganCB1.Items.SaveToFile(YeganNameFileAdd);
AddYeganTxt.Text:='';
end;

procedure TMainF.TntButton19Click(Sender: TObject);
begin
if AddAjansTxt.Text ='' then begin
  MMessageBox('��� ����� ���� ��� ���� ���.','���',MB_OK);
  exit;
end;
AddCBs('AjansCB',AddAjansTxt.Text);
AjansCB1.Items.SaveToFile(AjansNameFileAdd);
AddAjansTxt.Text:='';
end;

procedure TMainF.TntButton18Click(Sender: TObject);
begin
if MainF.Mmessagebox('�� ��� ��� ����� ��� ��� ������ �� ���� �� ��� ��� ����� ��� ��� ύ�� �Ԙ� ����. ��� ���� ����� ����� ����Ͽ','�����',MB_YESNO)=Mrno then exit;
if AnbarCB1.Text='' then begin
  MainF.Mmessagebox('������ ���� ��� ������ ����.','���',MB_OK);
  exit;
end;
RemoveFromCBs('AnbarCB',AnbarCB1.Text);
AnbarCB1.Items.SaveToFile(AnbarNameFileAdd);
AnbarCB1.ItemIndex:=0;
end;

procedure TMainF.TntButton26Click(Sender: TObject);
begin
if MainF.Mmessagebox('�� ��� ��� ����� ��� ��� ������ �� ���� �� ��� ��� ����� ��� ��� ύ�� �Ԙ� ����. ��� ���� ����� ����� ����Ͽ','�����',MB_YESNO)=Mrno then exit;
if YeganCB1.Text='' then begin
  MainF.Mmessagebox('������ ���� ��� ������ ����.','���',MB_OK);
  exit;
end;
RemoveFromCBs('YeganCB',YeganCB1.Text);
YeganCB1.Items.SaveToFile(YeganNameFileAdd);
YeganCB1.ItemIndex:=0;
end;

procedure TMainF.TntButton27Click(Sender: TObject);
begin
if MainF.Mmessagebox('�� ��� ��� ����� ��� ��� ������ �� ���� �� ��� ��� ����� ��� ��� ύ�� �Ԙ� ����. ��� ���� ����� ����� ����Ͽ','�����',MB_YESNO)=Mrno then exit;
if AjansCB1.Text='' then begin
  MainF.Mmessagebox('������ ���� ��� ������ ����.','���',MB_OK);
  exit;
end;
RemoveFromCBs('AjansCB',AjansCB1.Text);
AjansCB1.Items.SaveToFile(AjansNameFileAdd);
AjansCB1.ItemIndex:=0;
end;

procedure TMainF.TanzimTarikhClick(Sender: TObject);
begin
//
if (MonthCB1.ItemIndex>5)and(DayCB1.ItemIndex>29) then begin
  mmessagebox('����� ����� ���� ��� ��� ������ ���. ������ ������ ������.','���',MB_OK);
  exit;
  end;

MmessageBox('������ �� ��� ������ ������ ����� ����� ��� ��ʡ ���� ����� ����� �� �� ����� ��� ����� ���� �� ����� ������ ����� ��� �'+
    chr(13)+'�� �� ����� ����� �����''����� ���'' �� ������ ������:'
    +chr(13)+Datetostr(TFarDate.ShamsiToMilady(strtodate(YearED1.Text+'/'
    +inttostr(MonthCB1.ItemIndex+1)+'/'+inttostr(DayCB1.ItemIndex+1))))
    ,'����� �����',MB_OK);
DayCB1.ItemIndex:=TFarDate.fardayOf(TFarDate.MiladyToShamsi(now))-1;
MonthCB1.ItemIndex:=TFarDate.farMonthOf(TFarDate.MiladyToShamsi(now))-1;
YearED1.Text:=inttostr(TFarDate.farYearOf(TFarDate.MiladyToShamsi(now)));
end;

procedure TMainF.YearED1KeyPress(Sender: TObject; var Key: Char);
begin
if (ord(key)<48)or(ord(key)>57) then begin
  key:=chr(0);
  beep;
end;
end;

procedure TMainF.MonthPercentBut1KeyPress(Sender: TObject; var Key: Char);
begin
if (ord(key)<48)or(ord(key)>57) then begin
  key:=chr(0);
  beep;
end;
end;

procedure TMainF.TntButton9Click(Sender: TObject);
begin
NewMotorOfAjans.ShowModal;
end;

procedure TMainF.newbutClick(Sender: TObject);
begin
NewMotAndMash.DialogTyp:='Motor';
NewMotAndMash.ShowModal;
end;

procedure TMainF.TntButton1Click(Sender: TObject);
begin
NewMotAndMash.DialogTyp:='Mashin';
NewMotAndMash.ShowModal;
end;

procedure TMainF.TntButton4Click(Sender: TObject);
begin
NewMotAndMash.DialogTyp:='EslahMotor';
NewMotAndMash.ShowModal;
end;

procedure TMainF.TntButton5Click(Sender: TObject);
begin
NewMotAndMash.DialogTyp:='EslahMashin';
NewMotAndMash.ShowModal;
end;

procedure TMainF.TntButton7Click(Sender: TObject);
begin
if MainF.MotorDS.DataSet.Fields[10].AsString<>'' then begin
  MainF.Mmessagebox('��� ����� ���� ����� ���','���',MB_OK);
  exit;
end;
Tarkhis.DialogTyp:='Motor';
Tarkhis.ShowModal;
end;

procedure TMainF.TntButton28Click(Sender: TObject);
begin
AllAjansPrn.Execute;
end;

procedure TMainF.TntButton12Click(Sender: TObject);
begin
if ChkPass('��� � ј��� �� ���� ���') then
  AjansDS.DataSet.Delete
else
  MainF.Mmessagebox('��� ������ ���','���',MB_OK);
end;

procedure TMainF.TntButton11Click(Sender: TObject);
begin
if ChkPass('��� � ј��� �� ���� �������') then
  MotorDS.DataSet.Delete
else
  MainF.Mmessagebox('��� ������ ���','���',MB_OK);
end;

procedure TMainF.TntButton2Click(Sender: TObject);
begin
if ChkPass('��� � ј��� �� ���� �������') then
  MashinDS.DataSet.Delete
else
  MainF.Mmessagebox('��� ������ ���','���',MB_OK);
end;

procedure TMainF.TntButton22Click(Sender: TObject);
begin
AllMotorPrn.Execute;
end;

procedure TMainF.TntButton23Click(Sender: TObject);
begin
AllMashinPrn.Execute;
end;

function TMainF.RetNoInCBs(CBTyp, Meghdar: widestring): integer;
var
i:integer;
begin
result:=-1;

if CBTyp='NooZabtCB' then begin
  for i:=0 to NooZabtCB1.Items.Count-1 do begin
    if NooZabtCB1.Items[i]=Meghdar then begin
      result:=i;
      exit;
    end;
  end;
end

else if CBTyp='MamoreBargeZard' then begin
  for i:=0 to MamoreBargeZardCB1.Items.Count-1 do begin
    if MamoreBargeZardCB1.Items[i]=Meghdar then begin
      result:=i;
      exit;
    end;
  end;
end


else if CBTyp='AjansCB' then begin
  for i:=0 to AjansCB1.Items.Count-1 do begin
    if AjansCB1.Items[i]=Meghdar then begin
      result:=i;
      exit;
    end;
  end;
end

else if CBTyp='YearCB' then begin
  for i:=0 to YearCB1.Items.Count-1 do begin
    if YearCB1.Items[i]=Meghdar then begin
      result:=i;
      exit;
    end;
  end;
end

else if CBTyp='YeganCB' then begin
  for i:=0 to YeganCB1.Items.Count-1 do begin
    if YeganCB1.Items[i]=Meghdar then begin
      result:=i;
      exit;
    end;
  end;
end

else if CBTyp='AnbarCB' then begin
  for i:=0 to AnbarCB1.Items.Count-1 do begin
    if AnbarCB1.Items[i]=Meghdar then begin
      result:=i;
      exit;
    end;
  end;
end

else if CBTyp='NooZabt' then begin
  for i:=0 to NooZabtCB1.Items.Count-1 do begin
    if Meghdar=NooZabtCB1.Items[i] then begin
      result:=i;
      exit;
    end;
  end;
end

else begin
  Mmessagebox('��� �� ��� �� ��','ChkExistInCBs',MB_OK);
end;
end;

procedure TMainF.TntButton13Click(Sender: TObject);
begin
if AddNooZabt.Text ='' then begin
  MMessageBox('��� ����� ���� ��� ��� ���� ���.','���',MB_OK);
  exit;
end;
AddCBs('NooZabtCB',AddNooZabt.Text);
NooZabtCB1.Items.SaveToFile(AddNooZabtFileAdd);
AddNooZabt.Text:='';

end;

procedure TMainF.TntButton29Click(Sender: TObject);
begin
if MainF.Mmessagebox('�� ��� ��� ����� ��� ��� ������ �� ���� �� ��� ��� ����� ��� ��� ύ�� �Ԙ� ����. ��� ���� ����� ����� ����Ͽ','�����',MB_YESNO)=Mrno then exit;
if NooZabtCB6.Text='' then begin
  MainF.Mmessagebox('������ ���� ��� ������ ����.','���',MB_OK);
  exit;
end;
RemoveFromCBs('NooZabtCB',NooZabtCB6.Text);
NooZabtCB1.Items.SaveToFile(AjansNameFileAdd);
NooZabtCB6.ItemIndex:=0;
end;

procedure TMainF.TntButton10Click(Sender: TObject);
begin
MotorDS.Enabled := false;
if SearchFieldS.ItemIndex=0 then
  repeat
      MotorDBTable.Next
  until not((AnsiPos(SearchBCode.Text,MotorDBTable.FieldByName('CodeBrgeZard').AsString
            )=0)and(not MotorDBTable.Eof))

else if SearchFieldS.ItemIndex=1 then
  repeat
      MotorDBTable.Next
  until not((AnsiPos(SearchBCode.Text,MotorDBTable.FieldByName('ShomarePelak').AsString
            )=0)and(not MotorDBTable.Eof))

else if SearchFieldS.ItemIndex=2 then
  repeat
      MotorDBTable.Next
  until not((AnsiPos(SearchBCode.Text,MotorDBTable.FieldByName('ShomareMotor').AsString
            )=0)and(not MotorDBTable.Eof))

else if SearchFieldS.ItemIndex=3 then
  repeat
      MotorDBTable.Next
  until not((AnsiPos(SearchBCode.Text,
            MotorDBTable.FieldByName('ShomareBadane').AsString
            )=0)and(not MotorDBTable.Eof));

MotorDS.Enabled := true;
end;

procedure TMainF.TntButton3Click(Sender: TObject);
begin
MashinDS.Enabled := false;
if SearchFieldSMashin.ItemIndex=0 then
  repeat
      MashinDBTable.Next;
  until not((AnsiPos(SearchBCodeMashin.Text,
            MashinDBTable.FieldByName('CodeBrgeZard').AsString
            )=0)and(not MashinDBTable.Eof))

else if SearchFieldSMashin.ItemIndex=1 then
  repeat
      MashinDBTable.Next
  until not((AnsiPos(SearchBCodeMashin.Text,
            MashinDBTable.FieldByName('ShomarePelak').AsString
            )=0)and(not MashinDBTable.Eof))

else if SearchFieldSMashin.ItemIndex=2 then
  repeat
      MashinDBTable.Next
  until not((AnsiPos(SearchBCodeMashin.Text,
            MashinDBTable.FieldByName('ShomareMotor').AsString,
            )=0)and(not MashinDBTable.Eof))

else if SearchFieldSMashin.ItemIndex=3 then
  repeat
      MashinDBTable.Next
  until not ((AnsiPos(SearchBCodeMashin.Text,
            MashinDBTable.FieldByName('ShomareBadane').AsString
            )=0)and(not MashinDBTable.Eof));

MashinDS.Enabled := true;
end;

procedure TMainF.TntButton32Click(Sender: TObject);
begin
MotorDs.DataSet.First;
end;

procedure TMainF.TntButton31Click(Sender: TObject);
begin
MotorDs.DataSet.Last;
end;

procedure TMainF.TntButton33Click(Sender: TObject);
begin
MashinDS.DataSet.First;
end;

procedure TMainF.TntButton34Click(Sender: TObject);
begin
MashinDS.DataSet.Last;
end;

procedure TMainF.TntButton30Click(Sender: TObject);
begin
AjansDS.DataSet.Locate('AjansName',SearchAjans.Text,[loPartialKey]);
end;

procedure TMainF.TntButton36Click(Sender: TObject);
begin
AjansDS.DataSet.First;
end;

procedure TMainF.TntButton35Click(Sender: TObject);
begin
AjansDS.DataSet.Last;
end;

procedure TMainF.TntButton6Click(Sender: TObject);
begin
if MainF.MashinDS.DataSet.Fields[10].AsString<>'' then begin
  MainF.Mmessagebox('��� ����� ���� ����� ���','���',MB_OK);
  exit;
end;
Tarkhis.DialogTyp:='Mashin';
Tarkhis.ShowModal;
end;

procedure TMainF.TntButton15Click(Sender: TObject);
var
SumMot,SumMash:int64;
Point:integer;
Sal,Mon:word;
begin
if YearCB2.Text='' then begin
  MainF.Mmessagebox('�� ���� ������ ����.','���',MB_OK);
  exit;
end;
MainF.Mmessagebox('������� ��� ��� ��� ��� Ș�� ���� ����� ������.','�����',MB_OK);
SumMot:=0;
SumMash:=0;
Sal:=strtoint(YearCB2.Text);
if HP2.Checked then begin
  Point:=MainF.MotorDS.DataSet.RecNo;
  MainF.MotorDS.DataSet.First;
  if MainF.MotorDS.DataSet.Fields[11].AsString<>'' then
  if (TFarDate.farYearOf(TFarDate.farStrToDate(
      MainF.MotorDS.DataSet.Fields[10].AsString)) = Sal) then
    SumMot:=SumMot+MainF.MotorDS.DataSet.Fields[11].AsInteger;
  repeat
    MainF.MotorDS.DataSet.Next;
    if MainF.MotorDS.DataSet.Fields[11].AsString<>'' then
    if (TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MotorDS.DataSet.Fields[10].AsString)) = Sal) then
      SumMot:=SumMot+MainF.MotorDS.DataSet.Fields[11].AsInteger;
  until MainF.MotorDS.DataSet.RecNo=(MainF.MotorDS.DataSet.RecordCount);
  MainF.MotorDS.DataSet.RecNo:=Point;

  Point:=MainF.MashinDS.DataSet.RecNo;
  MainF.MashinDS.DataSet.First;
  if MainF.MotorDS.DataSet.Fields[11].AsString<>'' then 
  if (TFarDate.farYearOf(TFarDate.farStrToDate(
      MainF.MashinDS.DataSet.Fields[10].AsString)) = Sal) then

    SumMash:=SumMash+MainF.MashinDS.DataSet.Fields[11].AsInteger;

  repeat
    MainF.MashinDS.DataSet.Next;
    if MainF.MotorDS.DataSet.Fields[11].AsString<>'' then
    if (TFarDate.farYearOf(TFarDate.farStrtoDate(
        MainF.MashinDS.DataSet.Fields[10].AsString)) = Sal) then

      SumMash:=SumMash+MainF.MashinDS.DataSet.Fields[11].AsInteger;

  until MainF.MashinDS.DataSet.RecNo=(MainF.MashinDS.DataSet.RecordCount);
  MainF.MashinDS.DataSet.RecNo:=Point;

  SumMot:=SumMot*strtoint(MonthPercentBut2.Text) div 100;
  SumMash:=SumMash*strtoint(MonthPercentBut2.Text) div 100;
  MainF.Mmessagebox(MonthPercentBut2.Text+' ���� �� ����� ���� �� ����� ��ј�� �� ���'
    +YearCB2.Text+' �� ����� '+inttostr(SumMot)+' ���� � �� ����� '+inttostr(SumMash)
    +' ���� � �� ����� '+inttostr(SumMot+SumMash)+' ���� �� ����.','�����',MB_OK);
end;

if HP1.Checked then begin
  Mon:=HazineMonth.ItemIndex+1;
  Point:=MainF.MotorDS.DataSet.RecNo;
  MainF.MotorDS.DataSet.First;
  if MainF.MotorDS.DataSet.Fields[11].AsString<>'' then
  if (TFarDate.farYearOf(TFarDate.farStrtoDate(
      MainF.MotorDS.DataSet.Fields[10].AsString)) = Sal)
      and(TFarDate.farMonthOf(TFarDate.farStrtoDate(
      MainF.MotorDS.DataSet.Fields[10].AsString)) = Mon) then

    SumMot:=SumMot+MainF.MotorDS.DataSet.Fields[11].AsInteger;

  repeat
    MainF.MotorDS.DataSet.Next;
    if MainF.MotorDS.DataSet.Fields[11].AsString<>'' then
    if (TFarDate.farYearOf(TFarDate.farStrtoDate(
        MainF.MotorDS.DataSet.Fields[10].AsString)) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrtoDate(
        MainF.MotorDS.DataSet.Fields[10].AsString)) = Mon) then

      SumMot:=SumMot+MainF.MotorDS.DataSet.Fields[11].AsInteger;

  until MainF.MotorDS.DataSet.RecNo=(MainF.MotorDS.DataSet.RecordCount);
  MainF.MotorDS.DataSet.RecNo:=Point;

  Point:=MainF.MashinDS.DataSet.RecNo;
  MainF.MashinDS.DataSet.First;
  if MainF.MotorDS.DataSet.Fields[11].AsString<>'' then
  if (TFarDate.farYearOf(TFarDate.farStrtoDate(
      MainF.MashinDS.DataSet.Fields[10].AsString)) = Sal)
      and(TFarDate.farMonthOf(TFarDate.farStrtoDate(
      MainF.MashinDS.DataSet.Fields[10].AsString)) = Mon) then

    SumMash:=SumMash+MainF.MashinDS.DataSet.Fields[11].AsInteger;

  repeat
    MainF.MashinDS.DataSet.Next;
    if MainF.MotorDS.DataSet.Fields[11].AsString<>'' then
    if (TFarDate.farYearOf(TFarDate.farStrtoDate(
        MainF.MashinDS.DataSet.Fields[10].AsString)) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrtoDate(
        MainF.MashinDS.DataSet.Fields[10].AsString)) = Mon) then

      SumMash:=SumMash+MainF.MashinDS.DataSet.Fields[11].AsInteger;

  until MainF.MashinDS.DataSet.RecNo=(MainF.MashinDS.DataSet.RecordCount);
  MainF.MashinDS.DataSet.RecNo:=Point;

  SumMot:=SumMot*strtoint(MonthPercentBut1.Text) div 100;
  SumMash:=SumMash*strtoint(MonthPercentBut1.Text) div 100;
  MainF.Mmessagebox(MonthPercentBut1.Text+' ���� �� ����� ���� �� ����� ��ј�� �� ��� '+
    HazineMonth.Text+' �� ��� '+YearCB2.Text+' �� ����� '+inttostr(SumMot)
    +' �� ����� '+inttostr(SumMash)+' � �� ����� '+inttostr(SumMot+SumMash)
    +' ���� �� ����.','�����',MB_OK);
end;


end;


procedure TMainF.MonthPercentBut2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if MonthPercentBut1.Text<>'' then
  if strtoint(MonthPercentBut1.Text)>100 then begin
    MMessagebox('���� ����� ���� �� 100 ���� ����.','���',MB_OK);
    MonthPercentBut1.Text:='100';
  end;

end;

procedure TMainF.TntButton14Click(Sender: TObject);
var
Sum,Point,i:integer;
Mon,Sal:word;
begin
if YearCB1.Text='' then begin
  MainF.Mmessagebox('�� ���� ������ ����.','���',MB_OK);
  exit;
end;
MainF.Mmessagebox('������� ��� ��� ��� ��� Ș�� ���� ����� ������.','�����',MB_OK);

if AmarMotAndMash1.Checked=true then begin
  Sum:=0;
  if NooZabtCB1.Text='' then begin
    MainF.Mmessagebox('��� ��� ������ ����.','���',MB_OK);
    exit;
  end;
  Mon:=MotAndMashAmarMonth.ItemIndex+1;
  Sal:=strtoint(YearCB1.Text);

  if MotOrMash.ItemIndex=0 then begin
    MotorGozDBTable.Close;
    MotorGozDBTable.EmptyTable;
    MotorGozDBTable.Open;
    Point:=MainF.MotorDS.DataSet.RecNo;
    MainF.MotorDS.DataSet.First;
    i:=0;
    repeat
    i:=i+1;
    MainF.MotorDS.DataSet.RecNo:=i;
    if (MainF.MotorDS.DataSet.Fields[12].AsString=NooZabtCB1.Text)
        and(TFarDate.farYearOf(TFarDate.farStrtoDate(
        MainF.MotorDS.DataSet.Fields[3].AsString)) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrtoDate(
        MainF.MotorDS.DataSet.Fields[3].AsString)) = Mon) then begin

      if (Pelak1.ItemIndex=1)and(MainF.MotorDS.DataSet.Fields[4].AsString<>'0') then continue;    //BiPelak
      if (Pelak1.ItemIndex=2)and(MainF.MotorDS.DataSet.Fields[4].AsString='0') then continue;    //BaPelak

      if MainF.MotorDS.DataSet.Fields[10].AsString='' then
      MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          null,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ])
      else
      MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          MainF.MotorDS.DataSet.Fields[10].AsString,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ]);
      Sum:=Sum+1;
    end;
    until (i+1)>(MainF.MotorDS.DataSet.RecordCount);
    MainF.MotorDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� ���� '+MotOrMash.Text+'��� ����� ��� � ����� '+NooZabtCB1.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MotorGozPrn.SetParam('Onvan','���� '+MotOrMash.Text+'��� ����� ��� � ����� '+NooZabtCB1.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MotorGozPrn.Execute;
    end;
  end;

  if MotOrMash.ItemIndex=1 then begin
    MashinGozDBTable.Close;
    MashinGozDBTable.EmptyTable;
    MashinGozDBTable.Open;
    Point:=MainF.MashinDS.DataSet.RecNo;
    MainF.MashinDS.DataSet.First;
    i:=0;
    repeat
    i:=i+1;
    MainF.MashinDS.DataSet.RecNo:=i;
    if (MainF.MashinDS.DataSet.Fields[12].AsString=NooZabtCB1.Text)
        and(TFarDate.farYearOf(TFarDate.farStrtoDate(
        MainF.MashinDS.DataSet.Fields[3].AsString )) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrtoDate(
        MainF.MashinDS.DataSet.Fields[3].AsString )) = Mon) then begin

      if (Pelak1.ItemIndex=1)and(MainF.MashinDS.DataSet.Fields[4].AsString<>'0') then continue;    //BiPelak
      if (Pelak1.ItemIndex=2)and(MainF.MashinDS.DataSet.Fields[4].AsString='0') then continue;    //BaPelak


      if MainF.MashinDS.DataSet.Fields[10].AsString='' then
      MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          null,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ])
      else
      MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          MainF.MashinDS.DataSet.Fields[10].AsString,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ]);
      Sum:=Sum+1;
    end;
    until (i+1)>(MainF.MashinDS.DataSet.RecordCount);
    MainF.MashinDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� ���� '+MotOrMash.Text+'��� ����� ��� � ����� '+NooZabtCB1.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MashinGozPrn.SetParam('Onvan','���� '+MotOrMash.Text+'��� ����� ��� � ����� '+NooZabtCB1.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MashinGozPrn.Execute;
    end;
  end;
end

else if AmarMotAndMash2.Checked=true then begin
  Sum:=0;
  if NooZabtCB2.Text='' then begin
    MainF.Mmessagebox('��� ��� ������ ����.','���',MB_OK);
    exit;
  end;
  Mon:=MotAndMashAmarMonth.ItemIndex+1;
  Sal:=strtoint(YearCB1.Text);

  if MotOrMash.ItemIndex=0 then begin
    MotorGozDBTable.Close;
    MotorGozDBTable.EmptyTable;
    MotorGozDBTable.Open;
    Point:=MainF.MotorDS.DataSet.RecNo;
    MainF.MotorDS.DataSet.First;
    i:=0;
    repeat
    i:=i+1;
    MainF.MotorDS.DataSet.RecNo:=i;
    if MainF.MotorDS.DataSet.Fields[11].AsString<>'' then
    if ((MainF.MotorDS.DataSet.Fields[12].AsString=NooZabtCB2.Text)
        and(TFarDate.farYearOf(TFarDate.farStrtoDate(
        MainF.MotorDS.DataSet.Fields[10].AsString)) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrtoDate(
        MainF.MotorDS.DataSet.Fields[10].AsString)) = Mon)) then begin

      if (Pelak2.ItemIndex=1)and(MainF.MotorDS.DataSet.Fields[4].AsString<>'0') then continue;    //BiPelak
      if (Pelak2.ItemIndex=2)and(MainF.MotorDS.DataSet.Fields[4].AsString='0') then continue;    //BaPelak

      if MainF.MotorDS.DataSet.Fields[10].AsString='' then
      MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          null,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ])
      else
            MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          MainF.MotorDS.DataSet.Fields[10].AsString,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ]);
      Sum:=Sum+1;
    end;
    until (i+1)>(MainF.MotorDS.DataSet.RecordCount);
    MainF.MotorDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� ��� '+NooZabtCB2.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MotorGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� ��� '+NooZabtCB2.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MotorGozPrn.Execute;
    end;
  end;

  if MotOrMash.ItemIndex=1 then begin
    MashinGozDBTable.Close;
    MashinGozDBTable.EmptyTable;
    MashinGozDBTable.Open;
    Point:=MainF.MashinDS.DataSet.RecNo;
    MainF.MashinDS.DataSet.First;
    i:=0;
    repeat
    i:=i+1;
    MainF.MashinDS.DataSet.RecNo:=i;
    if MainF.MashinDS.DataSet.Fields[11].AsString<>'' then
    if (MainF.MashinDS.DataSet.Fields[12].AsString=NooZabtCB2.Text)
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[10].AsString)) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[10].AsString)) = Mon) then begin

      if (Pelak2.ItemIndex=1)and(MainF.MashinDS.DataSet.Fields[4].AsString<>'0') then continue;    //BiPelak
      if (Pelak2.ItemIndex=2)and(MainF.MashinDS.DataSet.Fields[4].AsString='0') then continue;    //BaPelak

      if MainF.MashinDS.DataSet.Fields[10].AsString='' then
      MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          null,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ])
      else
      MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          MainF.MashinDS.DataSet.Fields[10].AsString,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ]);
      Sum:=Sum+1;
    end;
    until (i+1)>(MainF.MashinDS.DataSet.RecordCount);
    MainF.MashinDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� ��� '+NooZabtCB2.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MashinGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� ��� '+NooZabtCB2.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MashinGozPrn.Execute;
    end;
  end;
end

else if AmarMotAndMash3.Checked=true then begin
  Sum:=0;
  if NooZabtCB3.Text='' then begin
    MainF.Mmessagebox('��� ��� ������ ����.','���',MB_OK);
    exit;
  end;
  Mon:=MotAndMashAmarMonth.ItemIndex+1;
  Sal:=strtoint(YearCB1.Text);

  if MotOrMash.ItemIndex=0 then begin
    MotorGozDBTable.Close;
    MotorGozDBTable.EmptyTable;
    MotorGozDBTable.Open;
    Point:=MainF.MotorDS.DataSet.RecNo;
    MainF.MotorDS.DataSet.First;
    i:=0;
    repeat
    i:=i+1;
    MainF.MotorDS.DataSet.RecNo:=i;
    if MainF.MotorDS.DataSet.Fields[11].AsString='' then
    if (MainF.MotorDS.DataSet.Fields[12].AsString=NooZabtCB3.Text)
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MotorDS.DataSet.Fields[3].AsString)) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrToDate(
        MainF.MotorDS.DataSet.Fields[3].AsString)) = Mon) then begin

      if (Pelak3.ItemIndex=1)and(MainF.MotorDS.DataSet.Fields[4].AsString<>'0') then continue;    //BiPelak
      if (Pelak3.ItemIndex=2)and(MainF.MotorDS.DataSet.Fields[4].AsString='0') then continue;    //BaPelak

      if MainF.MotorDS.DataSet.Fields[10].AsString='' then
      MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          null,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ])
      else
      MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          MainF.MotorDS.DataSet.Fields[10].AsString,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ]);
      Sum:=Sum+1;
    end;
    until (i+1)>(MainF.MotorDS.DataSet.RecordCount);
    MainF.MotorDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� '+NooZabtCB3.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MotorGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� '+NooZabtCB3.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MotorGozPrn.Execute;
    end;
  end;

  if MotOrMash.ItemIndex=1 then begin
    MashinGozDBTable.Close;
    MashinGozDBTable.EmptyTable;
    MashinGozDBTable.Open;
    Point:=MainF.MashinDS.DataSet.RecNo;
    MainF.MashinDS.DataSet.First;
    i:=0;
    repeat
    i:=i+1;
    MainF.MashinDS.DataSet.RecNo:=i;
    if MainF.MashinDS.DataSet.Fields[11].AsString='' then
    if (MainF.MashinDS.DataSet.Fields[12].AsString=NooZabtCB3.Text)
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[3].AsString )) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[3].AsString )) = Mon) then begin

      if (Pelak3.ItemIndex=1)and(MainF.MashinDS.DataSet.Fields[4].AsString<>'0') then continue;    //BiPelak
      if (Pelak3.ItemIndex=2)and(MainF.MashinDS.DataSet.Fields[4].AsString='0') then continue;    //BaPelak

      if MainF.MashinDS.DataSet.Fields[10].AsString='' then
      MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          null,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ])
      else
            MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          MainF.MashinDS.DataSet.Fields[10].AsString,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ]);
      Sum:=Sum+1;
    end;
    until (i+1)>(MainF.MashinDS.DataSet.RecordCount);
    MainF.MashinDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� '+NooZabtCB3.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MashinGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� '+NooZabtCB3.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MashinGozPrn.Execute;
    end;
  end;
end

else if AmarMotAndMash4.Checked=true then begin
  Sum:=0;
  if NooZabtCB4.Text='' then begin
    MainF.Mmessagebox('��� ��� ������ ����.','���',MB_OK);
    exit;
  end;
  if AnbarCB2.Text='' then begin
    MainF.Mmessagebox('����� ������ ����.','���',MB_OK);
    exit;
  end;
  Mon:=MotAndMashAmarMonth.ItemIndex+1;
  Sal:=strtoint(YearCB1.Text);

  if MotOrMash.ItemIndex=0 then begin
    MotorGozDBTable.Close;
    MotorGozDBTable.EmptyTable;
    MotorGozDBTable.Open;
    Point:=MainF.MotorDS.DataSet.RecNo;
    MainF.MotorDS.DataSet.First;
    i:=0;
    repeat
    i:=i+1;
    MainF.MotorDS.DataSet.RecNo:=i;
    if (MainF.MotorDS.DataSet.Fields[12].AsString=NooZabtCB4.Text)
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MotorDS.DataSet.Fields[3].AsString)) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrToDate(
        MainF.MotorDS.DataSet.Fields[3].AsString)) = Mon)
        and (AnbarCB2.Text=MainF.MotorDS.DataSet.Fields[8].AsString)
        and (MainF.MotorDS.DataSet.Fields[11].AsString='') then begin

      if (Pelak4.ItemIndex=1)and(MainF.MotorDS.DataSet.Fields[4].AsString<>'0') then continue;    //BiPelak
      if (Pelak4.ItemIndex=2)and(MainF.MotorDS.DataSet.Fields[4].AsString='0') then continue;    //BaPelak

      if MainF.MotorDS.DataSet.Fields[10].AsString='' then
      MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          null,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ])
      else
            MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          MainF.MotorDS.DataSet.Fields[10].AsString,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ]);
      Sum:=Sum+1;
    end;
    until (i+1)>(MainF.MotorDS.DataSet.RecordCount);
    MainF.MotorDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� '+NooZabtCB4.Text+
          ' �� ����� '+AnbarCB2.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MotorGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� '+NooZabtCB4.Text+
          ' �� ����� '+AnbarCB2.Text+' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MotorGozPrn.Execute;
    end;
  end;

  if MotOrMash.ItemIndex=1 then begin
    MashinGozDBTable.Close;
    MashinGozDBTable.EmptyTable;
    MashinGozDBTable.Open;
    Point:=MainF.MashinDS.DataSet.RecNo;
    MainF.MashinDS.DataSet.First;
    i:=0;
    repeat
    i:=i+1;
    MainF.MashinDS.DataSet.RecNo:=i;
    if (MainF.MashinDS.DataSet.Fields[12].AsString=NooZabtCB4.Text)
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[3].AsString)) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[3].AsString)) = Mon)
        and (AnbarCB2.Text=MainF.MotorDS.DataSet.Fields[8].AsString)
        and (MainF.MashinDS.DataSet.Fields[11].AsString='') then begin

      if (Pelak4.ItemIndex=1)and(MainF.MashinDS.DataSet.Fields[4].AsString<>'0') then continue;    //BiPelak
      if (Pelak4.ItemIndex=2)and(MainF.MashinDS.DataSet.Fields[4].AsString='0') then continue;    //BaPelak

      if MainF.MashinDS.DataSet.Fields[10].AsString='' then
      MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          null,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ])
      else
            MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          MainF.MashinDS.DataSet.Fields[10].AsString,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ]);

      Sum:=Sum+1;
    end;
    until (i+1)>(MainF.MashinDS.DataSet.RecordCount);
    MainF.MashinDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� '+NooZabtCB3.Text+
          ' �� ����� '+AnbarCB2.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MashinGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� '+NooZabtCB3.Text+
          ' �� ����� '+AnbarCB2.Text+' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MashinGozPrn.Execute;
    end;
  end;
end

else if AmarMotAndMash5.Checked=true then begin
  Sum:=0;
  if YeganCB2.Text='' then begin
    MainF.Mmessagebox('���� ������ ����.','���',MB_OK);
    exit;
  end;
  Mon:=MotAndMashAmarMonth.ItemIndex+1;
  Sal:=strtoint(YearCB1.Text);

  if MotOrMash.ItemIndex=0 then begin
    MotorGozDBTable.Close;
    MotorGozDBTable.EmptyTable;
    MotorGozDBTable.Open;
    Point:=MainF.MotorDS.DataSet.RecNo;
    MainF.MotorDS.DataSet.First;
    i:=1;
    repeat
    MainF.MotorDS.DataSet.RecNo:=i;
    if (MainF.MotorDS.DataSet.Fields[7].AsString=YeganCB2.Text)
        and (TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MotorDS.DataSet.Fields[3].AsString)) = Sal)
        and (TFarDate.farMonthOf(TFarDate.farStrToDate(
        MainF.MotorDS.DataSet.Fields[3].AsString)) = Mon) then begin
      if MainF.MotorDS.DataSet.Fields[10].AsString='' then
      MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          null,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ])
      else
            MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          MainF.MotorDS.DataSet.Fields[10].AsString,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ]);

      Sum:=Sum+1;
    end;
    i:=i+1;
    until i>(MainF.MotorDS.DataSet.RecordCount);
    MainF.MotorDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� ��� ���� '+YeganCB2.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MotorGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� ��� ���� '+YeganCB2.Text
          +' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MotorGozPrn.Execute;
    end;
  end;

  if MotOrMash.ItemIndex=1 then begin
    MashinGozDBTable.Close;
    MashinGozDBTable.EmptyTable;
    MashinGozDBTable.Open;
    Point:=MainF.MashinDS.DataSet.RecNo;
    MainF.MashinDS.DataSet.First;
    i:=1;
    repeat
    MainF.MashinDS.DataSet.RecNo:=i;
    if (MainF.MashinDS.DataSet.Fields[7].AsString=YeganCB2.Text)
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[3].AsString)) = Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[3].AsString)) = Mon) then begin
      if MainF.MashinDS.DataSet.Fields[10].AsString='' then
      MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          null,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ])
      else
            MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          MainF.MashinDS.DataSet.Fields[10].AsString,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ]);
      Sum:=Sum+1;
    end;
    i:=i+1;
    until i>(MainF.MashinDS.DataSet.RecordCount);
    MainF.MashinDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� ��� ���� '+YeganCB2.Text+
          ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MashinGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� ��� ���� '+YeganCB2.Text+
      ' �� ��� '+MotAndMashAmarMonth.Text+' �� ��� '+YearCB1.Text);
      MashinGozPrn.Execute;
    end;
  end;
end

else if AmarMotAndMash6.Checked=true then begin
  Sum:=0;

  if MotOrMash.ItemIndex=0 then begin
    MotorGozDBTable.Close;
    MotorGozDBTable.EmptyTable;
    MotorGozDBTable.Open;
    Point:=MainF.MotorDS.DataSet.RecNo;
    MainF.MotorDS.DataSet.First;
    i:=1;
    repeat
    MainF.MotorDS.DataSet.RecNo:=i;
    if (MainF.MotorDS.DataSet.Fields[10].AsString='') then begin
      MotorGozDBTable.AppendRecord([
          MainF.MotorDS.DataSet.Fields[0].AsString,
          MainF.MotorDS.DataSet.Fields[1].AsString,
          MainF.MotorDS.DataSet.Fields[2].AsString,
          MainF.MotorDS.DataSet.Fields[3].AsString,
          MainF.MotorDS.DataSet.Fields[4].AsString,
          MainF.MotorDS.DataSet.Fields[5].AsString,
          MainF.MotorDS.DataSet.Fields[6].AsString,
          MainF.MotorDS.DataSet.Fields[7].AsString,
          MainF.MotorDS.DataSet.Fields[8].AsString,
          MainF.MotorDS.DataSet.Fields[9].AsString,
          null,
          MainF.MotorDS.DataSet.Fields[11].AsString,
          MainF.MotorDS.DataSet.Fields[12].AsString,
          MainF.MotorDS.DataSet.Fields[13].AsString,
          MainF.MotorDS.DataSet.Fields[14].AsString,
          MainF.MotorDS.DataSet.Fields[15].AsString,
          MainF.MotorDS.DataSet.Fields[16].AsString,
          MainF.MotorDS.DataSet.Fields[17].AsString,
          MainF.MotorDS.DataSet.Fields[18].AsString,
          MainF.MotorDS.DataSet.Fields[19].AsString,
          MainF.MotorDS.DataSet.Fields[20].AsString,
          MainF.MotorDS.DataSet.Fields[21].AsString,
          MainF.MotorDS.DataSet.Fields[22].AsString,
          MainF.MotorDS.DataSet.Fields[23].AsString,
          MainF.MotorDS.DataSet.Fields[24].AsString,
          MainF.MotorDS.DataSet.Fields[25].AsString,
          MainF.MotorDS.DataSet.Fields[26].AsString,
          MainF.MotorDS.DataSet.Fields[27].AsString,
          MainF.MotorDS.DataSet.Fields[28].AsString,
          MainF.MotorDS.DataSet.Fields[29].AsString,
          MainF.MotorDS.DataSet.Fields[30].AsString,
          MainF.MotorDS.DataSet.Fields[31].AsString,
          MainF.MotorDS.DataSet.Fields[32].AsString,
          MainF.MotorDS.DataSet.Fields[33].AsString,
          MainF.MotorDS.DataSet.Fields[34].AsString,
          MainF.MotorDS.DataSet.Fields[35].AsString,
          MainF.MotorDS.DataSet.Fields[36].AsString
          ]);
      Sum:=Sum+1;
    end;
    i:=i+1;
    until i>(MainF.MotorDS.DataSet.RecordCount);
    MainF.MotorDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� �� �� ������� ���� ������� �����'+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MotorGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� �� �� ������� ���� ������� �����');
      MotorGozPrn.Execute;
    end;
  end;

  if MotOrMash.ItemIndex=1 then begin
    MashinGozDBTable.Close;
    MashinGozDBTable.EmptyTable;
    MashinGozDBTable.Open;
    Point:=MainF.MashinDS.DataSet.RecNo;
    MainF.MashinDS.DataSet.First;
    i:=1;
    repeat
    MainF.MashinDS.DataSet.RecNo:=i;
    if (MainF.MashinDS.DataSet.Fields[10].AsString='') then begin
      MashinGozDBTable.AppendRecord([
          MainF.MashinDS.DataSet.Fields[0].AsString,
          MainF.MashinDS.DataSet.Fields[1].AsString,
          MainF.MashinDS.DataSet.Fields[2].AsString,
          MainF.MashinDS.DataSet.Fields[3].AsString,
          MainF.MashinDS.DataSet.Fields[4].AsString,
          MainF.MashinDS.DataSet.Fields[5].AsString,
          MainF.MashinDS.DataSet.Fields[6].AsString,
          MainF.MashinDS.DataSet.Fields[7].AsString,
          MainF.MashinDS.DataSet.Fields[8].AsString,
          MainF.MashinDS.DataSet.Fields[9].AsString,
          null,
          MainF.MashinDS.DataSet.Fields[11].AsString,
          MainF.MashinDS.DataSet.Fields[12].AsString,
          MainF.MashinDS.DataSet.Fields[13].AsString,
          MainF.MashinDS.DataSet.Fields[14].AsString,
          MainF.MashinDS.DataSet.Fields[15].AsString,
          MainF.MashinDS.DataSet.Fields[16].AsString,
          MainF.MashinDS.DataSet.Fields[17].AsString,
          MainF.MashinDS.DataSet.Fields[18].AsString,
          MainF.MashinDS.DataSet.Fields[19].AsString,
          MainF.MashinDS.DataSet.Fields[20].AsString,
          MainF.MashinDS.DataSet.Fields[21].AsString,
          MainF.MashinDS.DataSet.Fields[22].AsString,
          MainF.MashinDS.DataSet.Fields[23].AsString,
          MainF.MashinDS.DataSet.Fields[24].AsString,
          MainF.MashinDS.DataSet.Fields[25].AsString,
          MainF.MashinDS.DataSet.Fields[26].AsString
          ]);
      Sum:=Sum+1;
    end;
    i:=i+1;
    until i>(MainF.MashinDS.DataSet.RecordCount);
    MainF.MashinDS.DataSet.RecNo:=Point;
    if  MainF.Mmessagebox('�� ����� ���� '+MotOrMash.Text+' ��� ����� �� �� ������� ���� ������� �����'+
          ' ����� '+inttostr(sum)+' ����� ���� ��.'+chr(13)+'��� ���� �� �ǁ ���� ���� ����Ͽ'
          ,'�����',MB_YESNO)=MRYES then begin
      MashinGozPrn.SetParam('Onvan',MotOrMash.Text+' ��� ����� �� �� ������� ���� ������� �����');
      MashinGozPrn.Execute;
    end;
  end;
end;


end;

procedure TMainF.MotorDBTableAfterPost(DataSet: TDataSet);
begin
MotRecCount.Caption:=inttostr(MotorDBTable.RecordCount);
end;

procedure TMainF.MotorDBTableAfterOpen(DataSet: TDataSet);
begin
MotRecCount.Caption:=inttostr(MotorDBTable.RecordCount);

end;

procedure TMainF.MotorDBTableAfterRefresh(DataSet: TDataSet);
begin
MotRecCount.Caption:=inttostr(MotorDBTable.RecordCount);
end;

procedure TMainF.MashinDBTableAfterOpen(DataSet: TDataSet);
begin
MashRecCount.Caption:=inttostr(MashinDBTable.RecordCount);

end;

procedure TMainF.MashinDBTableAfterPost(DataSet: TDataSet);
begin
MashRecCount.Caption:=inttostr(MashinDBTable.RecordCount);

end;

procedure TMainF.MashinDBTableAfterRefresh(DataSet: TDataSet);
begin
MashRecCount.Caption:=inttostr(MashinDBTable.RecordCount);

end;

procedure TMainF.TntButton8Click(Sender: TObject);
var
i,point,MotSum:integer;
Mon,Sal,Day:word;
HazSum:int64;

begin
if AjansCB3.Text='' then begin
  MainF.Mmessagebox('��� ��� ������ ����.','���',MB_OK);
  exit;
end;
if YearCB3.Text='' then begin
  MainF.Mmessagebox('��� ������ ����.','���',MB_OK);
  exit;
end;
MainF.Mmessagebox('������� ��� ��� ��� ��� Ș�� ���� ����� ������.','�����',MB_OK);

AjansGozDBTable.Close;
AjansGozDBTable.EmptyTable;
AjansGozDBTable.Open;
Point:=AjansDBTable.RecNo;
AjansDBTable.First;

Sal:=strtoint(YearCB3.Text);
i:=1;
MotSum:=0;
HazSum:=0;
if HM1.Checked then begin
  Mon:=MonthHM1.ItemIndex+1;
  Day:=DayHM1.ItemIndex+1;
  repeat
    AjansDBTable.RecNo:=i;
    if (AjansDBTable.Fields[0].AsString=AjansCB3.Text)and
       (TFarDate.farYearOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString)) = Sal)and
       (TFarDate.farMonthOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString))=Mon)and
       (TFarDate.fardayOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString))=Day) then begin
      AjansGozDBTable.AppendRecord([
        AjansDBTable.fields[0],
        AjansDBTable.fields[1],
        AjansDBTable.fields[2],
        AjansDBTable.fields[3],
        AjansDBTable.fields[4]
        ]);
    MotSum:=MotSum+AjansDBTable.fields[2].AsInteger;
    HazSum:=HazSum+AjansDBTable.fields[4].AsInteger;
    end;
    i:=i+1;
  until i>AjansDBTable.RecordCount;
  AjansDBTable.RecNo:=Point;
  if MainF.Mmessagebox('�� ����� �������� ������ ���� ��� '+
      AjansCB3.Text+' �� ��� '+YearCB3.Text+' � �� ��� '+MonthHM1.Text
      +' �� ��� '+DayHM1.Text+' ����� '+
      inttostr(MotSum)+' ����� �� ����� � '+inttostr(HazSum)+
      ' ���� ��. ��� ���� �� �ǁ ��� ����� �� ����Ͽ '
      ,'�����',MB_YESNO)=MRYES then begin
    AjansGozPrn.SetParam('Onvan',' �������� ������ ���� ��� '+
      AjansCB3.Text+' �� ��� '+YearCB3.Text+' � �� ��� '+MonthHM1.Text
      +' �� ��� '+DayHM1.Text);
    AjansGozPrn.Execute;
  end;
end

else if HM2.Checked then begin
  Mon:=MonthHM2.ItemIndex+1;
  Day:=DayHM2.ItemIndex+1;
  repeat
    AjansDBTable.RecNo:=i;
    if (AjansDBTable.Fields[0].AsString=AjansCB3.Text)and
       (TFarDate.farYearOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString))=Sal)and
       (TFarDate.farMonthOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString))=Mon)and
       (TFarDate.fardayOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString))>=Day)and
       (TFarDate.fardayOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString))<Day+7)then begin
      AjansGozDBTable.AppendRecord([
        AjansDBTable.fields[0],
        AjansDBTable.fields[1],
        AjansDBTable.fields[2],
        AjansDBTable.fields[3],
        AjansDBTable.fields[4]
        ]);
    MotSum:=MotSum+AjansDBTable.fields[2].AsInteger;
    HazSum:=HazSum+AjansDBTable.fields[4].AsInteger;
    end;
    i:=i+1;
  until i>AjansDBTable.RecordCount;
  AjansDBTable.RecNo:=Point;
  if MainF.Mmessagebox('�� ����� �������� ������ ���� ��� '+
      AjansCB3.Text+' �� ��� '+YearCB3.Text+' � �� ��� '+MonthHM2.Text
      +' �� ���� �� ��� '+DayHM2.Text+' ����� '+
      inttostr(MotSum)+' ����� �� ����� � '+inttostr(HazSum)+
      ' ���� ��. ��� ���� �� �ǁ ��� ����� �� ����Ͽ '
      ,'�����',MB_YESNO)=MRYES then begin
    AjansGozPrn.SetParam('Onvan',' �������� ������ ���� ��� '+
      AjansCB3.Text+' �� ��� '+YearCB3.Text+' � �� ��� '+MonthHM2.Text
      +' �� ���� �� ��� '+DayHM2.Text);
    AjansGozPrn.Execute;
  end;
end

else if HM3.Checked then begin
  Mon:=MonthHM3.ItemIndex+1;
  repeat
    AjansDBTable.RecNo:=i;
    if (AjansDBTable.Fields[0].AsString=AjansCB3.Text)and
       (TFarDate.farYearOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString))=Sal)and
       (TFarDate.farMonthOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString))=Mon) then begin
      AjansGozDBTable.AppendRecord([
        AjansDBTable.fields[0],
        AjansDBTable.fields[1],
        AjansDBTable.fields[2],
        AjansDBTable.fields[3],
        AjansDBTable.fields[4]
        ]);
    MotSum:=MotSum+AjansDBTable.fields[2].AsInteger;
    HazSum:=HazSum+AjansDBTable.fields[4].AsInteger;
    end;
    i:=i+1;
  until i>AjansDBTable.RecordCount;
  AjansDBTable.RecNo:=Point;
  if MainF.Mmessagebox('�� ����� �������� ������ ���� ��� '+
      AjansCB3.Text+' �� ��� '+YearCB3.Text+' � �� ��� '+MonthHM3.Text
      +' ����� '+inttostr(MotSum)+' ����� �� ����� � '+inttostr(HazSum)+
      ' ���� ��. ��� ���� �� �ǁ ��� ����� �� ����Ͽ '
      ,'�����',MB_YESNO)=MRYES then begin
    AjansGozPrn.SetParam('Onvan',' �������� ������ ���� ��� '+
      AjansCB3.Text+' �� ��� '+YearCB3.Text+' � �� ��� '+MonthHM3.Text);
    AjansGozPrn.Execute;
  end;
end

else if HM4.Checked then begin
  repeat
    AjansDBTable.RecNo:=i;
    if (AjansDBTable.Fields[0].AsString=AjansCB3.Text)and
       (TFarDate.farYearOf(TFarDate.farStrToDate(
          AjansDBTable.Fields[1].AsString))=Sal) then begin
      AjansGozDBTable.AppendRecord([
        AjansDBTable.fields[0],
        AjansDBTable.fields[1],
        AjansDBTable.fields[2],
        AjansDBTable.fields[3],
        AjansDBTable.fields[4]
        ]);
    MotSum:=MotSum+AjansDBTable.fields[2].AsInteger;
    HazSum:=HazSum+AjansDBTable.fields[4].AsInteger;
    end;
    i:=i+1;
  until i>AjansDBTable.RecordCount;
  AjansDBTable.RecNo:=Point;
  if MainF.Mmessagebox('�� ����� �������� ������ ���� ��� '+
      AjansCB3.Text+' �� ��� '+YearCB3.Text+' ����� '+
      inttostr(MotSum)+' ����� �� ����� � '+inttostr(HazSum)+
      ' ���� ��. ��� ���� �� �ǁ ��� ����� �� ����Ͽ '
      ,'�����',MB_YESNO)=MRYES then begin
    AjansGozPrn.SetParam('Onvan',' �������� ������ ���� ��� '+
      AjansCB3.Text+' �� ��� '+YearCB3.Text);
    AjansGozPrn.Execute;
  end;
end;

end;

procedure TMainF.TntButton43Click(Sender: TObject);
begin
TahvileBaregZardDB.Locate('CodeBrgeZard',BargeZardSearch.Text,[loPartialKey]);
end;

procedure TMainF.TntButton24Click(Sender: TObject);
begin
TahvileBaregZardDB.First;
end;

procedure TMainF.TntButton39Click(Sender: TObject);
begin
TahvileBaregZardDB.Last;
end;

procedure TMainF.TntButton41Click(Sender: TObject);
begin
if ChkPass('��� � ј��� �� �ѐ� ��� ����� ���') then
  TahvileBaregZardDB.Delete;
end;

procedure TMainF.TntButton42Click(Sender: TObject);
begin
AllTahvileBaregZardPrn.Execute;
end;

procedure TMainF.TntButton40Click(Sender: TObject);
begin
TahvilBargeZardF.ShowModal;
end;

procedure TMainF.TntButton38Click(Sender: TObject);
begin
if MainF.Mmessagebox('�� ��� ��� ����� ��� ��� ������ �� ���� �� ��� ��� ����� ��� ��� ύ�� �Ԙ� ����. ��� ���� ����� ����� ����Ͽ','�����',MB_YESNO)=Mrno then exit;
if NooZabtCB6.Text='' then begin
  MainF.Mmessagebox('������ ���� ��� ������ ����.','���',MB_OK);
  exit;
end;
RemoveFromCBs('MamoreBargeZard',MamoreBargeZardCB1.Text);
MamoreBargeZardCB1.Items.SaveToFile(MamoreBargeZardFileAdd);
MamoreBargeZardCB1.ItemIndex:=0;

end;

procedure TMainF.TntButton37Click(Sender: TObject);
begin
if AddMamoreBargeZard.Text ='' then begin
  MMessageBox('��� ����� ���� ��� ���� ���� ���.','���',MB_OK);
  exit;
end;
AddCBs('MamoreBargeZard',AddMamoreBargeZard.Text);
MamoreBargeZardCB1.Items.SaveToFile(MamoreBargeZardFileAdd);
AddMamoreBargeZard.Text:='';

end;

procedure TMainF.TntButton25Click(Sender: TObject);
begin
MotorDBTable.Close;
MotorGozDBTable.Close;
MashinDBTable.Close;
MashinGozDBTable.Close;
AjansDBTable.Close;
AjansGozDBTable.Close;
TahvileBaregZardDB.Close;
TahvileBaregZardGozDBTable.Close;

Application.Terminate;
end;

procedure TMainF.TntButton44Click(Sender: TObject);
var
Point,i,Sum:integer;
Sal,Mon,Day:word;
begin
if MamoreBargeZardCB2.Text='' then begin
  MainF.Mmessagebox('��� ���� ������ ���� ���.','���',MB_OK);
  exit;
end;

if YearCB4.Text='' then begin
  MainF.Mmessagebox('��� ������ ���� ���.','���',MB_OK);
  exit;
end;

MainF.Mmessagebox('������� ��� ��� ��� ��� Ș�� ���� ����� ������.','�����',MB_OK);

TahvileBaregZardGozDBTable.Close;
TahvileBaregZardGozDBTable.EmptyTable;
TahvileBaregZardGozDBTable.Open;

Point:=TahvileBaregZardDB.RecNo;
TahvileBaregZardDB.First;
i:=1;
Sal:=strtoint(YearCB4.Text);
Sum:=0;

if TBZ1.Checked then begin
  Mon:=MonTBZ1.ItemIndex+1;
  Day:=DayTBZ1.ItemIndex+1;
  repeat
    TahvileBaregZardDB.RecNo:=i;
    if (TFarDate.farYearOf(TFarDate.farStrToDate(
          TahvileBaregZardDB.Fields[7].AsString))=Sal)and
       (TFarDate.farMonthOf(TFarDate.farStrToDate(
          TahvileBaregZardDB.Fields[7].AsString))=Mon)and
       (TFarDate.farDayOf(TFarDate.farStrToDate(
          TahvileBaregZardDB.Fields[7].AsString))=Day) then begin
      TahvileBaregZardGozDBTable.AppendRecord([
        TahvileBaregZardDB.Fields[0].AsString,
        TahvileBaregZardDB.Fields[1].AsString,
        TahvileBaregZardDB.Fields[2].AsString,
        TahvileBaregZardDB.Fields[3].AsString,
        TahvileBaregZardDB.Fields[4].AsString,
        TahvileBaregZardDB.Fields[5].AsString,
        TahvileBaregZardDB.Fields[6].AsString,
        TahvileBaregZardDB.Fields[7].AsString,
        TahvileBaregZardDB.Fields[8].AsString
        ]);
      Sum:=Sum+1;
    end;
    i:=i+1;
  until i>TahvileBaregZardDB.RecordCount;
  TahvileBaregZardDB.RecNo:=Point;

  if MainF.Mmessagebox('�� ����� ���� �ѐ ��� ������ �� ���� '+
      MamoreBargeZardCB2.Text+' �� ��� '+YearCB4.Text+' ��� '+
      MonTBZ1.Text+' ��� '+DayTBZ1.Text+' ����� '+inttostr(Sum)
      +' �ѐ� ���� ��. ��� ���� �� �ǁ ����� ����Ͽ','�����',MB_YESNO)
      =MRYES then begin
    TahvileBaregZardGozPrn.SetParam('Onvan','�ѐ ��� ������ �� ���� '+
      MamoreBargeZardCB2.Text+' �� ��� '+YearCB4.Text+' �� ��� '+MonTBZ1.Text
        +' ��� '+DayTBZ1.Text);
    TahvileBaregZardGozPrn.Execute;
  end;
end

else if TBZ2.Checked then begin
  Mon:=MonTBZ2.ItemIndex+1;
  repeat
    TahvileBaregZardDB.RecNo:=i;
    if (TFarDate.farYearOf(TFarDate.farStrToDate(
          TahvileBaregZardDB.Fields[7].AsString))=Sal)and
       (TFarDate.farMonthOf(TFarDate.farStrToDate(
          TahvileBaregZardDB.Fields[7].AsString))=Mon) then begin
      TahvileBaregZardGozDBTable.AppendRecord([
        TahvileBaregZardDB.Fields[0].AsString,
        TahvileBaregZardDB.Fields[1].AsString,
        TahvileBaregZardDB.Fields[2].AsString,
        TahvileBaregZardDB.Fields[3].AsString,
        TahvileBaregZardDB.Fields[4].AsString,
        TahvileBaregZardDB.Fields[5].AsString,
        TahvileBaregZardDB.Fields[6].AsString,
        TahvileBaregZardDB.Fields[7].AsString,
        TahvileBaregZardDB.Fields[8].AsString
        ]);
      Sum:=Sum+1;
    end;
    i:=i+1;
  until i>TahvileBaregZardDB.RecordCount;
  TahvileBaregZardDB.RecNo:=Point;

  if MainF.Mmessagebox('�� ����� ���� �ѐ ��� ������ �� ���� '+
      MamoreBargeZardCB2.Text+' �� ��� '+YearCB4.Text+' ��� '+
      MonTBZ2.Text+' ����� '+inttostr(Sum)+' �ѐ� ���� ��. ��� ���� �� �ǁ ����� ����Ͽ','�����',MB_YESNO)
      =MRYES then begin
    TahvileBaregZardGozPrn.SetParam('Onvan','�ѐ ��� ������ �� ���� '+
      MamoreBargeZardCB2.Text+' �� ��� '+YearCB4.Text+' ��� '+MonTBZ2.Text);
    TahvileBaregZardGozPrn.Execute;
  end;
end

else if TBZ3.Checked then begin
  repeat
    TahvileBaregZardDB.RecNo:=i;
    if (TFarDate.farYearOf(TFarDate.farStrToDate(
        TahvileBaregZardDB.Fields[7].AsString))=Sal) then begin
      TahvileBaregZardGozDBTable.AppendRecord([
        TahvileBaregZardDB.Fields[0].AsString,
        TahvileBaregZardDB.Fields[1].AsString,
        TahvileBaregZardDB.Fields[2].AsString,
        TahvileBaregZardDB.Fields[3].AsString,
        TahvileBaregZardDB.Fields[4].AsString,
        TahvileBaregZardDB.Fields[5].AsString,
        TahvileBaregZardDB.Fields[6].AsString,
        TahvileBaregZardDB.Fields[7].AsString,
        TahvileBaregZardDB.Fields[8].AsString
        ]);
      Sum:=Sum+1;
    end;
    i:=i+1;
  until i>TahvileBaregZardDB.RecordCount;
  TahvileBaregZardDB.RecNo:=Point;

  if MainF.Mmessagebox('�� ����� ���� �ѐ ��� ������ �� ���� '+
      MamoreBargeZardCB2.Text+' �� �� ��� '+YearCB4.Text+
      ' ����� '+inttostr(Sum)+' �ѐ� ���� ��. ��� ���� �� �ǁ ����� ����Ͽ','�����',MB_YESNO)
      =MRYES then begin
    TahvileBaregZardGozPrn.SetParam('Onvan','�ѐ ��� ������ �� ���� '+
      MamoreBargeZardCB2.Text+' �� �� ��� '+YearCB4.Text);
    TahvileBaregZardGozPrn.Execute;
  end;
end


end;

procedure TMainF.LMDGlobalHotKey1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (InputOutput.Visible and MainF.Active ) then
 TntButton7.Click;
end;

procedure TMainF.LMDGlobalHotKey2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (InputOutput.Visible and MainF.Active ) then
 TntButton6.Click;

end;

procedure TMainF.SearchBCodeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = VK_RETURN then tntbutton10.Click;
end;

procedure TMainF.SearchBCodeMashinKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = VK_RETURN then tntbutton3.Click;

end;

procedure TMainF.TntButton46Click(Sender: TObject);
var
SumMot,SumMash:int64;
Point:integer;
Sal,Mon:word;
begin
if YearCB5.Text='' then begin
  MainF.Mmessagebox('�� ���� ������ ����.','���',MB_OK);
  exit;
end;
MainF.Mmessagebox('������� ��� ��� ��� ��� Ș�� ���� ����� ������.','�����',MB_OK);

SumMot:=0;
SumMash:=0;
Sal:=strtoint(YearCB5.Text);

if MR2.Checked then begin
  Point:=MainF.MotorDS.DataSet.RecNo;
  MainF.MotorDS.DataSet.First;
  if (MainF.MotorDS.DataSet.Fields[11].AsString<>'')
      and(TFarDate.farYearOf(TFarDate.farStrToDate(
      MainF.MotorDS.DataSet.Fields[10].AsString))=Sal) then

    SumMot:=SumMot+Tarkhis.PaDays(MainF.MotorDS.DataSet.Fields[3].AsString,
            MainF.MotorDS.DataSet.Fields[10].AsString);

  repeat
    MainF.MotorDS.DataSet.Next;
    if (MainF.MotorDS.DataSet.Fields[11].AsString<>'')
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
          MainF.MotorDS.DataSet.Fields[10].AsString))=Sal) then

      SumMot:=SumMot+Tarkhis.PaDays(MainF.MotorDS.DataSet.Fields[3].AsString,
              MainF.MotorDS.DataSet.Fields[10].AsString);

  until MainF.MotorDS.DataSet.RecNo=(MainF.MotorDS.DataSet.RecordCount);

  MainF.MotorDS.DataSet.RecNo:=Point;

  Point:=MainF.MashinDS.DataSet.RecNo;
  MainF.MashinDS.DataSet.First;

  if (MainF.MashinDS.DataSet.Fields[11].AsString<>'')
      and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[10].AsString))=Sal) then

    SumMash:=SumMash+Tarkhis.PaDays(MainF.MashinDS.DataSet.Fields[3].AsString,
            MainF.MashinDS.DataSet.Fields[10].AsString);

  repeat
    MainF.MashinDS.DataSet.Next;
    if (MainF.MashinDS.DataSet.Fields[11].AsString<>'')
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[10].AsString))=Sal) then

    SumMash:=SumMash+Tarkhis.PaDays(MainF.MashinDS.DataSet.Fields[3].AsString,
            MainF.MashinDS.DataSet.Fields[10].AsString);

  until MainF.MashinDS.DataSet.RecNo=(MainF.MashinDS.DataSet.RecordCount);

  MainF.MashinDS.DataSet.RecNo:=Point;

  MainF.Mmessagebox('�� ����� ������ ���� ����� ����� ��� �� ��� '
    +YearCB2.Text+' ���� ������� '+inttostr(SumMot)+' ��� � ������� '+inttostr(SumMash)
    +' ��� � �� ����� '+inttostr(SumMot+SumMash)+' ��� �� ����.','�����',MB_OK);
end;

if MR1.Checked then begin
  Mon:=MRMonth.ItemIndex+1;
  Point:=MainF.MotorDS.DataSet.RecNo;
  MainF.MotorDS.DataSet.First;
  if (MainF.MotorDS.DataSet.Fields[11].AsString<>'')
      and(TFarDate.farYearOf(TFarDate.farStrToDate(
      MainF.MotorDS.DataSet.Fields[10].AsString))=Sal)
      and(TFarDate.farMonthOf(TFarDate.farStrToDate(
      MainF.MotorDS.DataSet.Fields[10].AsString))=Mon) then

    SumMot:=SumMot+Tarkhis.PaDays(MainF.MotorDS.DataSet.Fields[3].AsString,
            MainF.MotorDS.DataSet.Fields[10].AsString);

  repeat
    MainF.MotorDS.DataSet.Next;
    if (MainF.MotorDS.DataSet.Fields[11].AsString<>'')
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MotorDS.DataSet.Fields[10].AsString))=Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrToDate(
        MainF.MotorDS.DataSet.Fields[10].AsString))=Mon) then

    SumMot:=SumMot+Tarkhis.PaDays(MainF.MotorDS.DataSet.Fields[3].AsString,
            MainF.MotorDS.DataSet.Fields[10].AsString);

  until MainF.MotorDS.DataSet.RecNo=(MainF.MotorDS.DataSet.RecordCount);
  MainF.MotorDS.DataSet.RecNo:=Point;

  Point:=MainF.MashinDS.DataSet.RecNo;
  MainF.MashinDS.DataSet.First;
  if (MainF.MashinDS.DataSet.Fields[11].AsString<>'')
      and(TFarDate.farYearOf(TFarDate.farStrToDate(
      MainF.MashinDS.DataSet.Fields[10].AsString))=Sal)
      and(TFarDate.farMonthOf(TFarDate.farStrToDate(
      MainF.MashinDS.DataSet.Fields[10].AsString))=Mon) then

    SumMash:=SumMash+Tarkhis.PaDays(MainF.MashinDS.DataSet.Fields[3].AsString,
            MainF.MashinDS.DataSet.Fields[10].AsString);

  repeat
    MainF.MashinDS.DataSet.Next;
    if (MainF.MashinDS.DataSet.Fields[11].AsString<>'')
        and(TFarDate.farYearOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[10].AsString))=Sal)
        and(TFarDate.farMonthOf(TFarDate.farStrToDate(
        MainF.MashinDS.DataSet.Fields[10].AsString))=Mon) then

    SumMash:=SumMash+Tarkhis.PaDays(MainF.MashinDS.DataSet.Fields[3].AsString,
            MainF.MashinDS.DataSet.Fields[10].AsString);

  until MainF.MashinDS.DataSet.RecNo=(MainF.MashinDS.DataSet.RecordCount);
  MainF.MashinDS.DataSet.RecNo:=Point;

  MainF.Mmessagebox('�� ����� ������ ���� ����� ����� ��� �� ��� '+
    MRMonth.Text+' �� ��� '+YearCB2.Text+' ���� ������� '+inttostr(SumMot)
    +' ��� � ������� '+inttostr(SumMash)+' ��� � �� ����� '+inttostr(SumMot+SumMash)
    +' ��� �� ����.','�����',MB_OK);
end;

end;

procedure TMainF.Button1Click(Sender: TObject);
begin
MotorDBTable.ApplyUpdates;
end;

end.
