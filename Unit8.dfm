object TahvilBargeZardF: TTahvilBargeZardF
  Left = 346
  Top = 166
  BiDiMode = bdRightToLeft
  BorderStyle = bsDialog
  Caption = #1578#1581#1608#1610#1604' '#1576#1585#1711' '#1586#1585#1583' '#1580#1583#1610#1583
  ClientHeight = 153
  ClientWidth = 337
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object TntShape13: TTntShape
    Left = 8
    Top = 8
    Width = 321
    Height = 137
    Brush.Color = clBtnFace
    Pen.Color = clGray
    Pen.Style = psDot
    Shape = stRoundRect
  end
  object TntLabel1: TTntLabel
    Left = 256
    Top = 19
    Width = 60
    Height = 14
    Caption = #1705#1583' '#1576#1585#1711#1607' '#1586#1585#1583':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BqkGLw +BigGMQavBkc +BjIGMQYv:'
  end
  object TntLabel3: TTntLabel
    Left = 166
    Top = 19
    Width = 48
    Height = 14
    Caption = #1606#1575#1605' '#1575#1601#1587#1585':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BkYGJwZF +BicGQQYzBjE:'
  end
  object TntLabel8: TTntLabel
    Left = 54
    Top = 19
    Width = 55
    Height = 14
    Caption = #1606#1608#1593' '#1608#1587#1740#1604#1607':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BkYGSAY5 +BkgGMwbMBkQGRw:'
  end
  object TntLabel9: TTntLabel
    Left = 258
    Top = 63
    Width = 58
    Height = 14
    Caption = #1578#1575#1585#1740#1582' '#1578#1581#1608#1610#1604':'
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BioGJwYxBswGLg +BioGLQZIBkoGRA:'
  end
  object BargeZardCode: TTntEdit
    Left = 229
    Top = 36
    Width = 89
    Height = 22
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    TabOrder = 0
  end
  object DayED1: TTntEdit
    Left = 299
    Top = 80
    Width = 18
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 2
    TabOrder = 1
  end
  object MonthED1: TTntEdit
    Left = 275
    Top = 80
    Width = 18
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 2
    TabOrder = 2
  end
  object YearED2: TTntEdit
    Left = 235
    Top = 80
    Width = 34
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 4
    TabOrder = 3
  end
  object Today: TTntButton
    Left = 192
    Top = 80
    Width = 37
    Height = 20
    Caption = #1575#1605#1585#1608#1586
    TabOrder = 4
    OnClick = TodayClick
    Caption_UTF7 = '+BicGRQYxBkgGMg'
  end
  object TntButton1: TTntButton
    Left = 101
    Top = 107
    Width = 68
    Height = 25
    Cancel = True
    Caption = #1582#1585#1608#1580
    ModalResult = 1
    TabOrder = 5
    OnClick = TntButton1Click
    Caption_UTF7 = '+Bi4GMQZIBiw'
  end
  object TntButton9: TTntButton
    Left = 21
    Top = 107
    Width = 68
    Height = 25
    Caption = #1579#1576#1578
    Default = True
    TabOrder = 6
    OnClick = TntButton9Click
    Caption_UTF7 = '+BisGKAYq'
  end
  object Vasileh: TTntComboBox
    Left = 21
    Top = 35
    Width = 89
    Height = 22
    Style = csDropDownList
    Color = clWhite
    ItemHeight = 14
    ItemIndex = 0
    TabOrder = 7
    Text = #1605#1608#1578#1608#1585
    Items.WideStrings = (
      #1605#1608#1578#1608#1585
      #1605#1575#1588#1740#1606)
    Items.WideStrings_UTF7 = (
      '+BkUGSAYqBkgGMQ'
      '+BkUGJwY0BswGRg')
    Text_UTF7 = '+BkUGSAYqBkgGMQ'
  end
  object MamoreBargeZardCB3: TTntComboBox
    Left = 127
    Top = 35
    Width = 89
    Height = 22
    Style = csDropDownList
    Color = clWhite
    ItemHeight = 14
    TabOrder = 8
  end
end
