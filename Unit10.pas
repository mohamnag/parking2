unit Unit10;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TntStdCtrls, ExtCtrls, TntExtCtrls, CheckLst,
  TntCheckLst, UfarsiDate, DateUtils, DateConv;

type
  TNewMotAndMash = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    TntShape13: TTntShape;
    TntButton9: TTntButton;
    TntButton1: TTntButton;
    MotorVSList: TTntCheckListBox;
    MashinVSList: TTntCheckListBox;
    TntLabel1: TTntLabel;
    TntLabel2: TTntLabel;
    TntLabel3: TTntLabel;
    TntLabel4: TTntLabel;
    TntLabel5: TTntLabel;
    TntLabel6: TTntLabel;
    TntLabel7: TTntLabel;
    TntLabel8: TTntLabel;
    TntLabel9: TTntLabel;
    TntLabel10: TTntLabel;
    TntLabel13: TTntLabel;
    BargeZardCode: TTntEdit;
    MotorTyp: TTntEdit;
    MotorColor: TTntEdit;
    ShomarePlak: TTntEdit;
    ShomareBadane: TTntEdit;
    ShomareMotor: TTntEdit;
    NooZabtCB5: TTntComboBox;
    YeganCB1: TTntComboBox;
    AnbarCB1: TTntComboBox;
    Radif: TTntEdit;
    DayED1: TTntEdit;
    MonthED1: TTntEdit;
    YearED2: TTntEdit;
    TntLabel11: TTntLabel;
    Today: TTntButton;
    Tozihat: TTntMemo;
    TntLabel12: TTntLabel;
    TntLabel14: TTntLabel;
    TarikhTarkhis: TTntEdit;
    HazineDaryafti: TTntEdit;
    TntLabel15: TTntLabel;
    Onvan: TTntLabel;
    procedure FormShow(Sender: TObject);
    procedure RadifKeyPress(Sender: TObject; var Key: Char);
    procedure TntButton1Click(Sender: TObject);
    procedure TodayClick(Sender: TObject);
    procedure TntButton9Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure clear();
    procedure ReadMotorDat();
    procedure ReadMashinDat();
    { Private declarations }
  public
    DialogTyp:string;
    { Public declarations }
  end;

var
  NewMotAndMash: TNewMotAndMash;

implementation

uses Unit1;

{$R *.dfm}

procedure TNewMotAndMash.FormShow(Sender: TObject);
begin
clear;
if DialogTyp='Motor' then begin
  MotorVSList.Visible:=true;
  MashinVSList.Visible:=false;
  Tntlabel14.Visible:=false;
  TarikhTarkhis.Visible:=false;
  tntlabel15.Visible:=false;
  HazineDaryafti.Visible:=false;
  Onvan.Caption:='����� ����:';
  today.Click;
  Today.Visible:=true;
end
else if DialogTyp='Mashin' then begin
  MashinVSList.Visible:=true;
  MotorVSList.Visible:=false;
  Tntlabel14.Visible:=false;
  TarikhTarkhis.Visible:=false;
  tntlabel15.Visible:=false;
  HazineDaryafti.Visible:=false;
  Onvan.Caption:='����� ����:';
  today.Click;
  Today.Visible:=true;
end
else if DialogTyp='EslahMotor' then begin
  MotorVSList.Visible:=true;
  MashinVSList.Visible:=false;
  Onvan.Caption:='����� ������� �����:';
  Tntlabel14.Visible:=true;
  TarikhTarkhis.Visible:=true;
  tntlabel15.Visible:=true;
  HazineDaryafti.Visible:=true;
  ReadMotorDat;
  Today.Visible:=true;
  YearED2.ReadOnly:=false;
  MonthED1.ReadOnly:=false;
  DayED1.ReadOnly:=false;
end
else if DialogTyp='EslahMashin' then begin
  MotorVSList.Visible:=false;
  MashinVSList.Visible:=true;
  Tntlabel14.Visible:=true;
  TarikhTarkhis.Visible:=true;
  tntlabel15.Visible:=true;
  HazineDaryafti.Visible:=true;
  Onvan.Caption:='����� ������� �����:';
  ReadMashinDat;
  Today.Visible:=true;
  YearED2.ReadOnly:=false;
  MonthED1.ReadOnly:=false;
  DayED1.ReadOnly:=false;
end;

end;

procedure TNewMotAndMash.RadifKeyPress(Sender: TObject; var Key: Char);
begin
if (ord(key)<48)or(ord(key)>57) then begin
  key:=chr(0);
  beep;
end;

end;

procedure TNewMotAndMash.TntButton1Click(Sender: TObject);
begin
clear;
end;

procedure TNewMotAndMash.TodayClick(Sender: TObject);
begin
  DayED1.Text:=inttostr(dayOf(TFarDate.MiladyToShamsi(now)));
  MonthED1.Text:=inttostr(MonthOf(TFarDate.MiladyToShamsi(now)));
  YearED2.Text:=inttostr(TFarDate.farYearOf(TFarDate.MiladyToShamsi(now)));
end;

procedure TNewMotAndMash.TntButton9Click(Sender: TObject);
var
i:integer;
begin
if (BargeZardCode.Text='')or(MotorTyp.Text='')or(MotorColor.Text='')or
    (DayED1.Text='')or(MonthED1.Text='')or(YearED2.Text='')
    or(ShomareBadane.Text='')or(ShomareMotor.Text='')or(Radif.Text='')or
    (NooZabtCB5.Text='') then begin
  MainF.Mmessagebox('�� �� ������� �� �ѐ� ��ϡ ��� ����ѡ �� ����ѡ ����Ρ ����� ���� ����� ����ѡ ���� �� ��� ���� ��� ��� ј���� ��� ������ ��.','���',MB_OK);
  exit;
end;
if YeganCB1.Text='' then begin
  MainF.Mmessagebox('��� ���� ������ ��� ���� ���� ���ϡ ǐ� �� ���� ����� ����� ���� ��� �� ��� ������� ������ ������.','���',MB_OK);
  exit;
end;
if AnbarCB1.Text='' then begin
  MainF.Mmessagebox('��� ����� �� ����� ���� ���ϡ ǐ� �� ���� ������ ����� ���� ��� �� �� ������� ������ ������.','���',MB_OK);
  exit;
end;

if DialogTyp='Motor' then
  MainF.MotorDS.DataSet.AppendRecord([BargeZardCode.Text,MotorTyp.Text,MotorColor.Text,
  YearED2.Text+'/'+MonthED1.Text+'/'+DayED1.Text,
  ShomarePlak.Text,ShomareBadane.Text,ShomareMotor.Text,YeganCB1.Text,AnbarCB1.Text,
  strtoint(Radif.Text),null,null,NooZabtCB5.Text,MotorVSList.Checked[0],MotorVSList.Checked[1],MotorVSList.Checked[2],
  MotorVSList.Checked[3],MotorVSList.Checked[4],MotorVSList.Checked[5],MotorVSList.Checked[6],
  MotorVSList.Checked[7],MotorVSList.Checked[8],MotorVSList.Checked[9],MotorVSList.Checked[10]
  ,MotorVSList.Checked[11],MotorVSList.Checked[12],MotorVSList.Checked[13],MotorVSList.Checked[14],
  MotorVSList.Checked[15],MotorVSList.Checked[16],MotorVSList.Checked[17],MotorVSList.Checked[18]
  ,MotorVSList.Checked[19],MotorVSList.Checked[20],MotorVSList.Checked[21],MotorVSList.Checked[22],
  Tozihat.Text]);

if DialogTyp='EslahMotor' then begin
  if not MainF.ChkPass('����� ������') then begin
    MainF.Mmessagebox('��� ������ ��ʡ �� ������ ����� ���.','���',MB_OK);
    exit;
  end;
  MainF.MotorDS.DataSet.Edit;
  MainF.MotorDS.DataSet.Fields[0].AsString:=BargeZardCode.Text;
  MainF.MotorDS.DataSet.Fields[1].AsString:=MotorTyp.Text;
  MainF.MotorDS.DataSet.Fields[2].AsString:=MotorColor.Text;
  MainF.MotorDS.DataSet.Fields[3].AsString:=YearED2.Text+'/'
    +MonthED1.Text+'/'+DayED1.Text;
  MainF.MotorDS.DataSet.Fields[4].AsString:=ShomarePlak.Text;
  MainF.MotorDS.DataSet.Fields[5].AsString:=ShomareBadane.Text;
  MainF.MotorDS.DataSet.Fields[6].AsString:=ShomareMotor.Text;
  MainF.MotorDS.DataSet.Fields[7].AsString:=YeganCB1.Text;
  MainF.MotorDS.DataSet.Fields[8].AsString:=AnbarCB1.Text;
  MainF.MotorDS.DataSet.Fields[9].AsString:=Radif.Text;
  MainF.MotorDS.DataSet.Fields[10].AsString:=TarikhTarkhis.Text;
  MainF.MotorDS.DataSet.Fields[11].AsString:=HazineDaryafti.Text;
  MainF.MotorDS.DataSet.Fields[12].AsString:=NooZabtCB5.Text;
  for i:=0 to MotorVSList.Items.Count-1 do
    MainF.MotorDS.DataSet.Fields[i+13].AsBoolean:=MotorVSList.Checked[i];
  MainF.MotorDS.DataSet.Fields[36].AsString:=Tozihat.Text;
  MainF.MotorDS.DataSet.Post;
  NewMotAndMash.ModalResult:=MROK;
end;

if DialogTyp='Mashin' then
  MainF.MashinDS.DataSet.AppendRecord([BargeZardCode.Text,MotorTyp.Text,MotorColor.Text,
  YearED2.Text+'/'+MonthED1.Text+'/'+DayED1.Text,
  ShomarePlak.Text,ShomareBadane.Text,ShomareMotor.Text,YeganCB1.Text,AnbarCB1.Text,
  strtoint(Radif.Text),null,null,NooZabtCB5.Text,MotorVSList.Checked[0],MotorVSList.Checked[1],MotorVSList.Checked[2],
  MotorVSList.Checked[3],MotorVSList.Checked[4],MotorVSList.Checked[5],MotorVSList.Checked[6],
  MotorVSList.Checked[7],MotorVSList.Checked[8],MotorVSList.Checked[9],MotorVSList.Checked[10]
  ,MotorVSList.Checked[11],MotorVSList.Checked[12],Tozihat.Text]);

if DialogTyp='EslahMashin' then begin
  if not MainF.ChkPass('����� ������') then begin
    MainF.Mmessagebox('��� ������ ��ʡ �� ������ ����� ���.','���',MB_OK);
    exit;
  end;
  MainF.MashinDS.DataSet.Edit;
  MainF.MashinDS.DataSet.Fields[0].AsString:=BargeZardCode.Text;
  MainF.MashinDS.DataSet.Fields[1].AsString:=MotorTyp.Text;
  MainF.MashinDS.DataSet.Fields[2].AsString:=MotorColor.Text;
  MainF.MashinDS.DataSet.Fields[3].AsString:=YearED2.Text+'/'+MonthED1.Text+'/'+DayED1.Text;
  MainF.MashinDS.DataSet.Fields[4].AsString:=ShomarePlak.Text;
  MainF.MashinDS.DataSet.Fields[5].AsString:=ShomareBadane.Text;
  MainF.MashinDS.DataSet.Fields[6].AsString:=ShomareMotor.Text;
  MainF.MashinDS.DataSet.Fields[7].AsString:=YeganCB1.Text;
  MainF.MashinDS.DataSet.Fields[8].AsString:=AnbarCB1.Text;
  MainF.MashinDS.DataSet.Fields[9].AsString:=Radif.Text;
  MainF.MashinDS.DataSet.Fields[10].AsString:=TarikhTarkhis.Text;
  MainF.MashinDS.DataSet.Fields[11].AsString:=HazineDaryafti.Text;
  MainF.MashinDS.DataSet.Fields[12].AsString:=NooZabtCB5.Text;
  for i:=0 to MashinVSList.Items.Count-1 do
    MainF.MashinDS.DataSet.Fields[i+13].AsBoolean:=MashinVSList.Checked[i];
  MainF.MashinDS.DataSet.Fields[26].AsString:=Tozihat.Text;
  MainF.MashinDS.DataSet.Post;
  NewMotAndMash.ModalResult:=MROK;
end;


if not Mainf.ChkExistInCBs('YearCB',YearED2.Text) then begin
  MainF.AddCBs('YearCB',YearED2.Text);
  MainF.YearCB3.Items.SaveToFile(MainF.YearNameFileAdd);
end;
clear;
BargeZardCode.SetFocus;
end;

procedure TNewMotAndMash.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
clear;
end;

procedure TNewMotAndMash.clear;
var
i:integer;
begin
for i:=0 to MotorVSList.Items.Count-1 do
  MotorVSList.Checked[i]:=true;
for i:=0 to MashinVSList.Items.Count-1 do
  MashinVSList.Checked[i]:=true;
BargeZardCode.Text:='';
MotorTyp.Text:='';
MotorColor.Text:='';
ShomarePlak.Text:='';
ShomareBadane.Text:='';
ShomareMotor.Text:='';
YeganCB1.ItemIndex:=0;
AnbarCB1.ItemIndex:=0;
Radif.Text:='';
NooZabtCB5.ItemIndex:=0;
Tozihat.Lines.Clear;

end;

procedure TNewMotAndMash.ReadMotorDat;
var
i:integer;
begin
BargeZardCode.Text:=MainF.MotorDS.DataSet.Fields[0].AsString;
MotorTyp.Text:=MainF.MotorDS.DataSet.Fields[1].AsString;
MotorColor.Text:=MainF.MotorDS.DataSet.Fields[2].AsString;
DayED1.Text:=inttostr(TFarDate.farDayOf(TFarDate.farStrToDate(
  MainF.MotorDS.DataSet.Fields[3].AsString)));
MonthED1.Text:=inttostr(TFarDate.farMonthOf(TFarDate.farStrToDate(
  MainF.MotorDS.DataSet.Fields[3].AsString)));
YearED2.Text:=inttostr(TFarDate.farYearOf(TFarDate.farStrToDate(
MainF.MotorDS.DataSet.Fields[3].AsString)));
ShomarePlak.Text:=MainF.MotorDS.DataSet.Fields[4].AsString;
ShomareBadane.Text:=MainF.MotorDS.DataSet.Fields[5].AsString;
ShomareMotor.Text:=MainF.MotorDS.DataSet.Fields[6].AsString;
YeganCB1.ItemIndex:=MainF.RetNoInCBs('YeganCB',MainF.MotorDS.DataSet.Fields[7].AsString);
AnbarCB1.ItemIndex:=MainF.RetNoInCBs('AnbarCB',MainF.MotorDS.DataSet.Fields[8].AsString);
Radif.Text:=MainF.MotorDS.DataSet.Fields[9].AsString;
TarikhTarkhis.Text:=MainF.MotorDS.DataSet.Fields[10].AsString;
HazineDaryafti.Text:=MainF.MotorDS.DataSet.Fields[11].AsString;

for i:=0 to NooZabtCB5.Items.Count-1 do begin
  NooZabtCB5.ItemIndex:=i;
  if NooZabtCB5.Text=MainF.MotorDS.DataSet.Fields[12].AsString then
    break;
end;

for i:=0 to MotorVSList.Items.Count-1 do
  MotorVSList.Checked[i]:=MainF.MotorDS.DataSet.Fields[i+13].AsBoolean;
Tozihat.Text:=MainF.MotorDS.DataSet.Fields[36].AsString;

end;

procedure TNewMotAndMash.ReadMashinDat;
var
i:integer;
begin
BargeZardCode.Text:=MainF.MashinDS.DataSet.Fields[0].AsString;
MotorTyp.Text:=MainF.MashinDS.DataSet.Fields[1].AsString;
MotorColor.Text:=MainF.MashinDS.DataSet.Fields[2].AsString;
DayED1.Text:=inttostr(TFarDate.farDayOf(TFarDate.farStrToDate(
  MainF.MashinDS.DataSet.Fields[3].AsString)));
MonthED1.Text:=inttostr(TFarDate.farMonthOf(TFarDate.farStrToDate(
  MainF.MashinDS.DataSet.Fields[3].AsString)));
YearED2.Text:=inttostr(TFarDate.farYearOf(TFarDate.farStrToDate(
  MainF.MashinDS.DataSet.Fields[3].AsString)));
ShomarePlak.Text:=MainF.MashinDS.DataSet.Fields[4].AsString;
ShomareBadane.Text:=MainF.MashinDS.DataSet.Fields[5].AsString;
ShomareMotor.Text:=MainF.MashinDS.DataSet.Fields[6].AsString;
YeganCB1.ItemIndex:=MainF.RetNoInCBs('YeganCB',MainF.MashinDS.DataSet.Fields[7].AsString);
AnbarCB1.ItemIndex:=MainF.RetNoInCBs('AnbarCB',MainF.MashinDS.DataSet.Fields[8].AsString);
Radif.Text:=MainF.MashinDS.DataSet.Fields[9].AsString;
TarikhTarkhis.Text:=MainF.MashinDS.DataSet.Fields[10].AsString;
HazineDaryafti.Text:=MainF.MashinDS.DataSet.Fields[11].AsString;

for i:=0 to NooZabtCB5.Items.Count-1 do begin
  NooZabtCB5.ItemIndex:=i;
  if NooZabtCB5.Text=MainF.MashinDS.DataSet.Fields[12].AsString then
    break;
end;

for i:=0 to MashinVSList.Items.Count-1 do
  MashinVSList.Checked[i]:=MainF.MashinDS.DataSet.Fields[i+13].AsBoolean;
Tozihat.Text:=MainF.MashinDS.DataSet.Fields[26].AsString;

end;

end.
