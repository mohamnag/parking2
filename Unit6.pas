unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TntStdCtrls, ExtCtrls, TntExtCtrls, jpeg, IdeFileLock, md5
  ,registry, LMDCustomComponent, LMDOneInstance;

type
  TStartF = class(TForm)
    TntImage1: TTntImage;
    Timer1: TTimer;
    LMDOneInstance1: TLMDOneInstance;
    procedure Timer1Timer(Sender: TObject);
    procedure LMDOneInstance1Custom(Sender: TObject);
  private
    function ChkAcc():boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StartF: TStartF;

implementation

uses Unit1, Unit11;

{$R *.dfm}
    const Mag:string='��� ��� ����� ���� ��� �� ������ ���� ���� Ȑ����.';

function TStartF.ChkAcc():boolean;
var
Reg:Tregistry;
label rr;

begin

reg:=tregistry.Create;
try
  Reg.RootKey:= HKEY_LOCAL_MACHINE;
  Reg.OpenKey('SOFTWARE\IVS\MN\Parking', False);
  if md5print(MD5String(GetACFromID(GetFileAndSystemID(paramstr(0)))))
     <>Reg.ReadString('Acc') then begin
    ACForm.ACCode.Text:=GetFileAndSystemID(paramstr(0));
    rr:
    if ACForm.ShowModal=mrCancel then begin
      if Mainf.Mmessagebox('���� ���� ���� ��� ����� ����� ���� ��� ����. ��� ���� �� ���Ͽ','���',MB_YESNO)
          =MRYES then begin
        result:=false;
        exit;
      end;
      goto rr;
    end;
    if ACForm.InputCode.text=GetACFromID(GetFileAndSystemID(paramstr(0))) then begin
      reg.WriteString('Acc',md5print(MD5String(GetACFromID(GetFileAndSystemID(paramstr(0))))));
    end
    else begin
      if Mainf.Mmessagebox('�� ���� ��� ���� ����. ��� ����� ���� �� ���Ͽ','���',MB_YESNO)
          =MRNO then begin
        result:=false;
        exit;
      end;
      goto rr;
    end;
  end;
finally
  Reg.Free;
end;
result:=true;
end;

procedure TStartF.Timer1Timer(Sender: TObject);
label pre;
var
ProAdd:String;
begin
Timer1.Enabled:=false;

{if not ChkAcc() then begin
  application.Terminate;
  exit;
end;}

pre:
if not(MainF.ChkPass('���� �� ������')) then begin
  if (MainF.Mmessagebox('��� ���� ������ ��ʡ ���� �� ������ ��� ����.'+chr(13)
    +'���� ���� ���� ��� �� ������ ������.'+chr(13)+'��� �� ������ ���� �� ���Ͽ'
    ,'��� ������',MB_YESNO)=IDYES) then begin
      application.Terminate;
      exit;
    end;
goto pre;
end;

MainF.AnbarNameFileAdd:='ANNF.Dat';
MainF.AjansNameFileAdd:='AJNF.Dat';
MainF.YeganNameFileAdd:='YENF.dat';
MainF.YearNameFileAdd:='YNFA.dat';
MainF.AddNooZabtFileAdd:='NZ.dat';
MainF.MamoreBargeZardFileAdd:='MBZ.dat';

ProAdd:=extractFileDir(paramstr(0))+'\';

MainF.AjansNameFileAdd:=ProAdd+MainF.AjansNameFileAdd;
MainF.YeganNameFileAdd:=ProAdd+MainF.YeganNameFileAdd;
MainF.AnbarNameFileAdd:=ProAdd+MainF.AnbarNameFileAdd;
MainF.AddNooZabtFileAdd:=ProAdd+MainF.AddNooZabtFileAdd;
MainF.MamoreBargeZardFileAdd:=ProAdd+MainF.MamoreBargeZardFileAdd;

if not(fileexists(MainF.MamoreBargeZardFileAdd)) then begin
  MainF.Mmessagebox('�� ���� ��� ���� ����� ������ �ѐ ��� ���� ���� ���� ���� ���� ��� ��� �� ��� ������� ��� ��� ������.','�����',MB_OK);
end
else begin
  MainF.FileToCBs('MamoreBargeZard',MainF.MamoreBargeZardFileAdd);
end;


if not(fileexists(MainF.AddNooZabtFileAdd)) then begin
  MainF.Mmessagebox('�� ���� �� ��� ���� ���� ���� ���� ���� ���� ��� ��� �� ��� ������� ��� ��� ������.','�����',MB_OK);
end
else begin
  MainF.FileToCBs('NooZabtCB',MainF.AddNooZabtFileAdd);
end;


if not(fileexists(MainF.AjansNameFileAdd)) then begin
  //first run -> Tanzimat For Ajans
  MainF.Mmessagebox('�� ���� �� ��� ���� ���� ���� ���� ����� ��� ��� �� �� ��� ������� ������ ������','�����',MB_OK);
end
else begin
  MainF.FileToCBs('AjansCB',MainF.AjansNameFileAdd);
end;

if not(fileexists(MainF.YeganNameFileAdd)) then begin
  //first run -> Tanzimat For Yegan
  MainF.Mmessagebox('�� ���� �� ��� ���� ����� ����� ��� ���� ���� ���� ����� ��� ���� ��� ������ �� ��� ������� ������ ������.','�����',MB_OK);
end
else begin
  MainF.FileToCBs('YeganCB',MainF.YeganNameFileAdd);
end;

if not(fileexists(MainF.AnbarNameFileAdd)) then begin
  //first run -> Tanzimat For Anbar
  MainF.Mmessagebox('�� ���� ��� �� ������ ���� ���� ���� ����� ��� ������� �� ��� ������� ������ ������.','�����',MB_OK);
end
else begin
  MainF.FileToCBs('AnbarCB',MainF.AnbarNameFileAdd);
end;

if fileexists(MainF.YearNameFileAdd)then
  MainF.FileToCBs('YearCB',MainF.YearNameFileAdd);

MainF.MotorDBTable.Active:=true;
MainF.MashinDBTable.Active:=true;
MainF.AjansDBTable.Active:=true;
MainF.MotorGozDBTable.Active:=true;
MainF.MashinGozDBTable.Active:=true;
MainF.TahvileBaregZardDB.Open;

MainF.InOutBut.Click;
StartF.Hide;
MainF.Show;
end;

procedure TStartF.LMDOneInstance1Custom(Sender: TObject);
begin
ShowMessage('Parking 1 is already running!'+#13+'You can''t run another instance.');
//application.Terminate;
end;

end.
