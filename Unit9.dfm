object NewMotorOfAjans: TNewMotorOfAjans
  Left = 299
  Top = 258
  BiDiMode = bdRightToLeft
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' '#1578#1581#1608#1610#1604' '#1605#1608#1578#1608#1585' '#1580#1583#1610#1583' '#1578#1608#1587#1591' '#1570#1688#1575#1606#1587
  ClientHeight = 177
  ClientWidth = 465
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object TntShape13: TTntShape
    Left = 6
    Top = 8
    Width = 453
    Height = 163
    Brush.Color = clBtnFace
    Pen.Color = clGray
    Pen.Style = psDot
    Shape = stRoundRect
  end
  object TntLabel19: TTntLabel
    Left = 354
    Top = 135
    Width = 62
    Height = 14
    Caption = #1578#1575#1585#1740#1582' '#1578#1581#1608#1740#1604': '
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BioGJwYxBswGLg +BioGLQZIBswGRA: '
  end
  object TntLabel33: TTntLabel
    Left = 354
    Top = 106
    Width = 71
    Height = 14
    Caption = #1605#1576#1604#1594' '#1583#1585#1740#1575#1601#1578#1740': '
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BkUGKAZEBjo +Bi8GMQbMBicGQQYqBsw: '
  end
  object TntLabel34: TTntLabel
    Left = 264
    Top = 108
    Width = 19
    Height = 14
    Caption = #1585#1740#1575#1604
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BjEGzAYnBkQ'
  end
  object TntLabel32: TTntLabel
    Left = 354
    Top = 78
    Width = 81
    Height = 14
    Caption = #1606#1575#1605' '#1608#1575#1581#1583' '#1570#1608#1585#1606#1583#1607': '
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BkYGJwZF +BkgGJwYtBi8 +BiIGSAYxBkYGLwZH: '
  end
  object TntLabel31: TTntLabel
    Left = 354
    Top = 50
    Width = 99
    Height = 14
    Caption = #1578#1593#1583#1575#1583' '#1605#1608#1578#1608#1585' '#1578#1581#1608#1740#1604#1740': '
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BioGOQYvBicGLw +BkUGSAYqBkgGMQ +BioGLQZIBswGRAbM: '
  end
  object TntLabel30: TTntLabel
    Left = 354
    Top = 22
    Width = 64
    Height = 14
    Caption = #1575#1587#1605' '#1570#1688#1575#1606#1587': '
    Color = clBtnFace
    ParentColor = False
    Caption_UTF7 = '+BicGMwZF +BiIGmAYnBkYGMw: '
  end
  object TntButton9: TTntButton
    Left = 21
    Top = 136
    Width = 68
    Height = 25
    Caption = #1579#1576#1578
    Default = True
    TabOrder = 9
    OnClick = TntButton9Click
    Caption_UTF7 = '+BisGKAYq'
  end
  object Today: TTntButton
    Left = 221
    Top = 134
    Width = 37
    Height = 20
    Caption = #1575#1605#1585#1608#1586
    TabOrder = 7
    OnClick = TodayClick
    Caption_UTF7 = '+BicGRQYxBkgGMg'
  end
  object YearED2: TTntEdit
    Left = 268
    Top = 134
    Width = 34
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 4
    TabOrder = 6
  end
  object MonthED1: TTntEdit
    Left = 308
    Top = 134
    Width = 18
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 2
    TabOrder = 5
  end
  object DayED1: TTntEdit
    Left = 332
    Top = 134
    Width = 18
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 2
    TabOrder = 4
  end
  object Mablag: TTntEdit
    Left = 287
    Top = 105
    Width = 63
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    TabOrder = 3
  end
  object YeganCB3: TTntComboBox
    Left = 245
    Top = 75
    Width = 105
    Height = 20
    Style = csOwnerDrawFixed
    Color = clWhite
    ItemHeight = 14
    TabOrder = 2
  end
  object MotorNo: TTntEdit
    Left = 293
    Top = 48
    Width = 57
    Height = 20
    BevelInner = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    TabOrder = 1
    OnKeyPress = MotorNoKeyPress
  end
  object AjansCB2: TTntComboBox
    Left = 245
    Top = 19
    Width = 105
    Height = 22
    Style = csDropDownList
    Color = clWhite
    ItemHeight = 14
    TabOrder = 0
  end
  object TntButton1: TTntButton
    Left = 101
    Top = 136
    Width = 68
    Height = 25
    Cancel = True
    Caption = #1582#1585#1608#1580
    TabOrder = 8
    OnClick = TntButton1Click
    Caption_UTF7 = '+Bi4GMQZIBiw'
  end
end
